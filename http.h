/* 
 * File:   http.h
 * Author: Erik
 *
 * Created on September 14, 2015, 6:31 PM
 */

#ifndef HTTP_H
#define	HTTP_H

void ShutDownServer(pthread_t TopThread);
void * WebServerThread(void * ptr);

#endif	/* HTTP_H */

