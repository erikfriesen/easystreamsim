/* 
 * File:   RemControl.h
 * Author: Erik
 *
 * Created on October 16, 2017, 11:46 AM
 */

#ifndef REMCONTROL_H
#define	REMCONTROL_H

#ifdef	__cplusplus
extern "C" {
#endif

void * RemControlThread(void * ptr);


#ifdef	__cplusplus
}
#endif

#endif	/* REMCONTROL_H */

