/* 
 * File:   b64.h
 * Author: Erik
 *
 * Created on September 11, 2015, 10:17 AM
 */

#ifndef B64_H
#define	B64_H

#ifdef	__cplusplus
extern "C" {
#endif

	typedef enum {
		step_A, step_B, step_C
	} base64_encodestep;

	typedef struct {
		base64_encodestep step;
		char result;
		int stepcount;
	} base64_encodestate;

	void base64_init_encodestate(base64_encodestate* state_in);

	char base64_encode_value(char value_in);

	int base64_encode_block(const char* plaintext_in, int length_in, char* code_out, base64_encodestate* state_in);

	int base64_encode_blockend(char* code_out, base64_encodestate* state_in);


#ifdef	__cplusplus
}
#endif

#endif	/* B64_H */

