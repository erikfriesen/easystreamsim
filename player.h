/* 
 * File:   player.h
 * Author: Erik
 *
 * Created on September 21, 2015, 2:17 PM
 */

#ifndef PLAYER_H
#define	PLAYER_H

enum url_scheme {
  SCHEME_HTTP,
#ifdef HAVE_SSL
  SCHEME_HTTPS,
#endif
  SCHEME_FTP,
  SCHEME_INVALID
};

struct url123
{
  char *url;                /* Original URL */
  enum url_scheme scheme;   /* URL scheme */

  char *host;               /* Extracted hostname */
  int port;                 /* Port number */

  /* URL components (URL-quoted). */
  char *path;
  char *params;
  char *query;
  char *fragment;

  /* Extracted path info (unquoted). */
  char *dir;
  char *file;

  /* Username and password (unquoted). */
  char *user;
  char *passwd;
};

void * PlaybackThread(void * ptr);
void * PlaybackAudioThread(void * ptr);

#endif	/* PLAYER_H */

