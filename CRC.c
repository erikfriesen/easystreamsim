//crc routines
#include "CRC.h"

const unsigned short poly = 4129;
unsigned short table[256];
unsigned short initialValue = 0;

unsigned short ComputeChecksum(void * buff, int HowMany) {
    unsigned char * bytes = buff;
    volatile unsigned short crc = initialValue;
    int i;
    for (i = 0; i < HowMany; ++i) {
        crc = (unsigned short) ((crc << 8) ^ table[((crc >> 8) ^ (0xff & bytes[i]))]);
    }
    return crc;
}

void InitCRC(int InValue) {
    int i, j;
    initialValue = (unsigned short) InValue;
    unsigned short temp, a;
    for (i = 0; i < 256; ++i) {
        temp = 0;
        a = (unsigned short) (i << 8);
        for (j = 0; j < 8; ++j) {
            if (((temp ^ a) & 0x8000) != 0) {
                temp = (unsigned short) ((temp << 1) ^ poly);
            } else {
                temp <<= 1;
            }
            a <<= 1;
        }
        table[i] = temp;
    }
}