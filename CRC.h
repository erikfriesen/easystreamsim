/* 
 * File:   CRC.h
 * Author: Erik
 *
 * Created on April 29, 2014, 5:16 PM
 */

#ifndef CRC_H
#define	CRC_H

extern void InitCRC(int InValue);
extern unsigned short ComputeChecksum(void * buff, int HowMany);

#endif	/* CRC_H */

