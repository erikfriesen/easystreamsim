#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "include/GenericTypeDefs.h"
#include "json.h"
#include "CRC.h"
#include "config.h"
#include "iceconfig.h"
#include "global.h"
#include "configsave.h"
#include <sys/stat.h> 
#include <fcntl.h>

#define JsonConfigVersion "1.00"

#ifdef USE_USB_STORAGE
const char * ConfigPath = "/mnt/USB1/easystream.conf";
const char * BackupConfigPath = "/mnt/USB1/easystream.bak";
const char * ReadOnlyConfig = "/etc/ice/easystream.bak";
int ReadOnlyBackupConfig(char * buff, int maxlen);
#else
const char * ConfigPath = "easystream.bin";
const char * BackupConfigPath = "easystream.bak";
#endif

pthread_mutex_t eepromconfigmutex = PTHREAD_MUTEX_INITIALIZER;

char * BuildXmlStatus(int Complete) {
	char * buff = malloc(1024);
	int v;
	if (buff == NULL) {
		return NULL;
	}
	char * SPtr = buff;
	if (Complete > 0) {
		SPtr += sprintf(SPtr, "{\"Config\":");
	}
	if (Complete>-1) {
		SPtr += sprintf(SPtr, "{");
	}
	SPtr += sprintf(SPtr, "\"Info\":{");
	SPtr += sprintf(SPtr, "\"DeviceName\":\"%s\",", Info.DeviceName);
	SPtr += sprintf(SPtr, "\"DeviceType\":\"streamer\",");
	SPtr += sprintf(SPtr, "\"DeviceSerial\":\"%c%c%c:%c%c%c:%c%c%c\",",
			  Info.DeviceSerial[8], Info.DeviceSerial[7], Info.DeviceSerial[6],
			  Info.DeviceSerial[5], Info.DeviceSerial[4], Info.DeviceSerial[3],
			  Info.DeviceSerial[2], Info.DeviceSerial[1], Info.DeviceSerial[0]);
	SPtr += sprintf(SPtr, "\"AuthToken\":\"%s\",", Info.AuthToken);
	SPtr += sprintf(SPtr, "\"Streams\":[{");
	//Streams[0]
	SPtr += sprintf(SPtr, "\"Status\":");
	if (StreamSetup.Mounts[0].AuthError) {
		SPtr += sprintf(SPtr, "\"Bad Stream auth settings\",");
	} else if (StreamSetup.Mounts[0].StreamConnectErr) {
		switch (StreamSetup.Mounts[0].StreamConnectErr) {
			case StrmErr_NoHost:
				SPtr += sprintf(SPtr, "\"No host\",");
				break;
			case StrmErr_NoConnect:
				SPtr += sprintf(SPtr, "\"Unable to connect\",");
				break;
			case StrmErr_NoWrite:
				SPtr += sprintf(SPtr, "\"Unable to reach host\",");
				break;
			case StrmErr_Auth:
				SPtr += sprintf(SPtr, "\"Unable to authenticate\",");
				break;
			case StrmErr_LostConn:
				SPtr += sprintf(SPtr, "\"Connection lost\",");
				break;
		}
	} else if (StreamSetup.Mounts[0].StreamConnected) {
		SPtr += sprintf(SPtr, "\"Streaming to %s\",", StreamSetup.Mounts[0].Name);
	} else {
		SPtr += sprintf(SPtr, "\"Off\",");
	}
	SPtr += sprintf(SPtr, "\"Connected\":\"%i\",", StreamSetup.Mounts[0].StreamConnected);
	SPtr += sprintf(SPtr, "\"CtrlStream\":\"%i\"},{", StreamSetup.Mounts[0].Ctrl_Stream);
	//Streams[1]
	SPtr += sprintf(SPtr, "\"Status\":");
	if (StreamSetup.Mounts[1].AuthError || StreamSetup.Mounts[0].StreamConnectErr) {
		SPtr += sprintf(SPtr, "\"Bad Stream settings\",");
	} else if (StreamSetup.Mounts[1].StreamConnectErr) {
		switch (StreamSetup.Mounts[1].StreamConnectErr) {
			case StrmErr_NoHost:
				SPtr += sprintf(SPtr, "\"No host\",");
				break;
			case StrmErr_NoConnect:
				SPtr += sprintf(SPtr, "\"Unable to connect\",");
				break;
			case StrmErr_NoWrite:
				SPtr += sprintf(SPtr, "\"Unable to write\",");
				break;
			case StrmErr_Auth:
				SPtr += sprintf(SPtr, "\"Unable to authenticate\",");
				break;
			case StrmErr_LostConn:
				SPtr += sprintf(SPtr, "\"Connection lost\",");
				break;
		}
	} else if (StreamSetup.Mounts[1].StreamConnected) {
		SPtr += sprintf(SPtr, "\"Streaming to %s\",", StreamSetup.Mounts[1].Name);
	} else {
		SPtr += sprintf(SPtr, "\"Off\",");
	}
	SPtr += sprintf(SPtr, "\"Connected\":\"%i\",", StreamSetup.Mounts[1].StreamConnected);
	SPtr += sprintf(SPtr, "\"CtrlStream\":\"%i\"}],", StreamSetup.Mounts[1].Ctrl_Stream);
	SPtr += sprintf(SPtr, "\"PlayState\":\"%s\",", PlayerSetup.PlayState);
	SPtr += sprintf(SPtr, "\"EncType\":\"%i\",", StreamSetup.EncType);
	SPtr += sprintf(SPtr, "\"CtrlStreamIn\":\"%i\",", PlayerSetup.Ctrl_StreamIn);
	SPtr += sprintf(SPtr, "\"DspError\":\"%i\",", DspSettings.Error);
	v = ConvertToDb10(PeakDetector);
	SPtr += sprintf(SPtr, "\"AudioPeak\":\"%i.%i\",", v / 10, v % 10);
	v = ConvertToDb10(AudioLevel);
	SPtr += sprintf(SPtr, "\"AudioLevel\":\"%i.%i\",", v / 10, v % 10);
	SPtr += sprintf(SPtr, "\"PlayerBuffer\":\"%i\",", BuffInMs);
	SPtr += sprintf(SPtr, "\"Unixtime\":\"%i\",", Info.LastConfigStamp);
	SPtr += sprintf(SPtr, "\"JsonVersion\":\"%s\",", JsonConfigVersion);
	SysServices.ControlStatus[63] = 0;
	SPtr += sprintf(SPtr, "\"ControlStatus\":\"%s\",", SysServices.ControlStatus);
	SPtr += sprintf(SPtr, "\"Version\":\"%s\"}", EASYSTREAMVERSION);
	if (Complete>-1) {
		SPtr += sprintf(SPtr, "}");
	}
	if (Complete > 0) {
		SPtr += sprintf(SPtr, "}");
	}
	return buff;
}

char * BuildXmlConfig(void) {
	int a, f = 0;
	char * buff = malloc(16384);
	char * inf_buf = BuildXmlStatus(-1);
	if (buff == NULL || inf_buf == NULL) {
		if (inf_buf != NULL) {
			free(inf_buf);
		}
		if (buff != NULL) {
			free(buff);
		}
		return NULL;
	}
	char * SPtr = buff;
	SPtr += sprintf(SPtr, "{\"Config\":{");
	SPtr += sprintf(SPtr, "%s,\"Player\":{", inf_buf);
	SPtr += sprintf(SPtr, "\"CtrlStreamIn\":\"%i\",", PlayerSetup.Ctrl_StreamIn);
	SPtr += sprintf(SPtr, "\"CtrlRxSelection\":\"%i\",", PlayerSetup.Ctrl_RxSelection);
	SPtr += sprintf(SPtr, "\"PlayState\":\"%s\",", PlayerSetup.PlayState);
	SPtr += sprintf(SPtr, "\"RxStreams\":["); //Opener
	for (a = 0; a < 32; a++) {
		SPtr += sprintf(SPtr, "{\"Name\":\"%s\",", PlayerSetup.RxStreams[a].Name);
		SPtr += sprintf(SPtr, "\"Username\":\"%s\",", PlayerSetup.RxStreams[a].Username);
		SPtr += sprintf(SPtr, "\"Password\":\"%s\",", PlayerSetup.RxStreams[a].Password);
		SPtr += sprintf(SPtr, "\"Url\":\"%s\"},", PlayerSetup.RxStreams[a].Url);
	}
	SPtr -= 1; //Drop last comma   
	SPtr += sprintf(SPtr, "]},\"Stream\":{"); //Closer
	SPtr += sprintf(SPtr, "\"ControlType\":\"%i\",", StreamSetup.ControlType);
	SPtr += sprintf(SPtr, "\"BitRate\":\"%i\",", StreamSetup.BitRate);
	SPtr += sprintf(SPtr, "\"EncType\":\"%i\",", StreamSetup.EncType);
	SPtr += sprintf(SPtr, "\"StreamOnLevel\":\"%i\",", StreamSetup.StreamOnLevel);
	SPtr += sprintf(SPtr, "\"StreamOnDelay\":\"%i\",", StreamSetup.StreamOnDelay);
	SPtr += sprintf(SPtr, "\"Mounts\":[{"); //Opener
	SPtr += sprintf(SPtr, "\"StreamActive\":\"%i\",", StreamSetup.Mounts[0].StreamActive);
	SPtr += sprintf(SPtr, "\"CtrlStream\":\"%i\",", StreamSetup.Mounts[0].Ctrl_Stream);
	SPtr += sprintf(SPtr, "\"StreamConnected\":\"%i\",", StreamSetup.Mounts[0].StreamConnected);
	SPtr += sprintf(SPtr, "\"AuthError\":\"%i\",", StreamSetup.Mounts[0].AuthError);
	SPtr += sprintf(SPtr, "\"SourceName\":\"%s\",", StreamSetup.Mounts[0].SourceName);
	SPtr += sprintf(SPtr, "\"UserName\":\"%s\",", StreamSetup.Mounts[0].UserName);
	SPtr += sprintf(SPtr, "\"Password\":\"%s\",", StreamSetup.Mounts[0].Password);
	SPtr += sprintf(SPtr, "\"Name\":\"%s\",", StreamSetup.Mounts[0].Name);
	SPtr += sprintf(SPtr, "\"URL\":\"%s\",", StreamSetup.Mounts[0].URL);
	SPtr += sprintf(SPtr, "\"Port\":\"%i\"},{", StreamSetup.Mounts[0].Port);
	SPtr += sprintf(SPtr, "\"StreamActive\":\"%i\",", StreamSetup.Mounts[1].StreamActive);
	SPtr += sprintf(SPtr, "\"CtrlStream\":\"%i\",", StreamSetup.Mounts[1].Ctrl_Stream);
	SPtr += sprintf(SPtr, "\"StreamConnected\":\"%i\",", StreamSetup.Mounts[1].StreamConnected);
	SPtr += sprintf(SPtr, "\"AuthError\":\"%i\",", StreamSetup.Mounts[1].AuthError);
	SPtr += sprintf(SPtr, "\"SourceName\":\"%s\",", StreamSetup.Mounts[1].SourceName);
	SPtr += sprintf(SPtr, "\"UserName\":\"%s\",", StreamSetup.Mounts[1].UserName);
	SPtr += sprintf(SPtr, "\"Password\":\"%s\",", StreamSetup.Mounts[1].Password);
	SPtr += sprintf(SPtr, "\"Name\":\"%s\",", StreamSetup.Mounts[1].Name);
	SPtr += sprintf(SPtr, "\"URL\":\"%s\",", StreamSetup.Mounts[1].URL);
	SPtr += sprintf(SPtr, "\"Port\":\"%i\"}]},", StreamSetup.Mounts[1].Port);

	SPtr += sprintf(SPtr, "\"Services\":{"); //closer
	SPtr += sprintf(SPtr, "\"UpdateUrl\":\"%s\",", SysServices.UpdateUrl);
	SPtr += sprintf(SPtr, "\"DoUpdate\":\"%i\",", SysServices.DoUpdate);
	SPtr += sprintf(SPtr, "\"ControlUrl\":\"%s\",", SysServices.ControlUrl);
	SPtr += sprintf(SPtr, "\"RemoteControl\":\"%i\",", SysServices.RemoteControl);
	SPtr += sprintf(SPtr, "\"PreAuthToken\":\"%s\",", SysServices.PreAuthToken);
	SPtr += sprintf(SPtr, "\"PollPeriod\":\"%i\",", SysServices.PollPeriod);
	SysServices.UpdateStatus[63] = 0;
	SPtr += sprintf(SPtr, "\"UpdateStatus\":\"%s\",", SysServices.UpdateStatus);
	SysServices.ControlStatus[63] = 0;
	SPtr += sprintf(SPtr, "\"ControlStatus\":\"%s\"},", SysServices.ControlStatus);
	SPtr += sprintf(SPtr, "\"Hardware\":{");
	SPtr += sprintf(SPtr, "\"HardwareGain\":\"%i\",", HardwareSetup.HardwareGain);
	SPtr += sprintf(SPtr, "\"SampleRate\":\"%i\",", HardwareSetup.SampleRate);
	SPtr += sprintf(SPtr, "\"InVolume\":\"%f\",", HardwareSetup.InVolume);
	SPtr += sprintf(SPtr, "\"OutVolume\":\"%i\",", HardwareSetup.OutVolume);
	SPtr += sprintf(SPtr, "\"AutoLimiter\":\"%i\"},", HardwareSetup.AutoLimiter);
	SPtr += sprintf(SPtr, "\"DSP\":{");
	SPtr += sprintf(SPtr, "\"Type\":\"%s\",", DspSettings.Type == 0 ? "Symetrix" : "Unknown");
	SPtr += sprintf(SPtr, "\"IP\":\"%i.%i.%i.%i\",", DspSettings.IP.v[0], DspSettings.IP.v[1], DspSettings.IP.v[2], DspSettings.IP.v[3]);
	SPtr += sprintf(SPtr, "\"Port\":\"%i\",", DspSettings.Port);
	SPtr += sprintf(SPtr, "\"TCP\":\"%i\",", DspSettings.TCP);
	SPtr += sprintf(SPtr, "\"Stream1\":\"%i\",", DspSettings.Stream1);
	SPtr += sprintf(SPtr, "\"Stream2\":\"%i\",", DspSettings.Stream2);
	SPtr += sprintf(SPtr, "\"StreamIn\":\"%i\",", DspSettings.StreamIn);
	SPtr += sprintf(SPtr, "\"AudioLevel\":\"%i\",", DspSettings.AudioLevel);
	SPtr += sprintf(SPtr, "\"IsStreaming1\":\"%i\",", DspSettings.IsStreaming1);
	SPtr += sprintf(SPtr, "\"IsStreaming2\":\"%i\",", DspSettings.IsStreaming2);
	SPtr += sprintf(SPtr, "\"AudioPeak\":\"%i\",", DspSettings.AudioPeak);
	SPtr += sprintf(SPtr, "\"RxSelection\":\"%i\",", DspSettings.RxSelection);
	SPtr += sprintf(SPtr, "\"Error\":\"%i\"}", DspSettings.Error);
	SPtr += sprintf(SPtr, "}}");
	free(inf_buf);
	return buff;
}

int SaveCurrentConfig(void) {
	int r = 1;
	pthread_mutex_lock(&eepromconfigmutex);
	{
		char * ConfigXml = BuildXmlConfig();
		FILE * WriteConfig = fopen(ConfigPath, "w");
		if (WriteConfig == NULL) {
			MyLog("Unable to create configuration %s\n", strerror(errno));
			r = 0;
			goto badfinish;
		}
		size_t len = strlen(ConfigXml);
		unsigned short CRC = ComputeChecksum(ConfigXml, len);
		fwrite((char*) &CRC, 1, 2, WriteConfig);
		fwrite(ConfigXml, 1, strlen(ConfigXml), WriteConfig);
		fclose(WriteConfig);

		FILE * WriteBackup = fopen(BackupConfigPath, "w");
		if (WriteBackup == NULL) {
			MyLog("Unable to create Backup configuration %s\n", strerror(errno));
			r = 0;
			goto badfinish;
		}
		if (WriteBackup != NULL) {
			fwrite((char*) &CRC, 1, 2, WriteBackup);
			fwrite(ConfigXml, 1, len, WriteBackup);
			fclose(WriteBackup);
		}
badfinish:
		free(ConfigXml);
	}
	pthread_mutex_unlock(&eepromconfigmutex);
	return r;
}

int ConfigManager(void) {

	int len;
	json_value* value;
	char json[32767];
	memset(json, 0, 32767);
	len = ReadMainConfig(json, 32767);
	if (len < 1) {
		len = ReadBackupConfig(json, 32767);
	}
#ifdef USE_USB_STORAGE
	if (len < 1) {
		Debug("Trying readonly config");
		len = ReadOnlyBackupConfig(json, 4096);
		if (len > 0) {
			Debug("Attempting to use ReadOnly config");
		}
	}
#endif
	if (len < 1) {
		if (SaveNewConfig()) {
			Debug("Built new config\n");
			return 0;
		}
		Debug("Could not save config\n");
		return 1;
	}

	value = json_parse(json, len);
	if (value == NULL) {
		MyLog("Garbage in main config\n");
		len = ReadBackupConfig(json, 32767);
		if (len > 0) {
			value = json_parse(json, len);
		}
		if (value == NULL) {
			MyLog("Garbage in second config\n");
			if (SaveNewConfig()) {
				Debug("Built new configuration\n");
				return 0;
			}
		}
	}
	FillConfigInfo(value);
	json_value_free(value);
	return 0;
}

#ifdef USE_EEPROM

int ReadMainConfig(char * buff, int maxlen) {
	unsigned short FileCRC;
	size_t size = 0;
	if (EEPROMReadArray(0, &size, sizeof (size_t)) < 0) {
		MyLog("Unable to read config size #1\n");
		return -2;
	}
	if (EEPROMReadArray(sizeof (size_t), &FileCRC, 2) < 0) {
		MyLog("Unable to read config crc #1\n");
		return -2;
	}
	if (size > MaxConfigLen) {
		Debug("size > %i %i\n", MaxConfigLen, size);
		return 0;
	}
	if (EEPROMReadArray(sizeof (size_t) + 2, buff, size) < 0) {
		MyLog("Unable to read config #1\n");
		return -2;
	}
	//Check crc
	if (ComputeChecksum(buff, size) != FileCRC) {
		MyLog("Config #1 CRC mismatch\n");
		return -2;
	}
	return size;
}

int ReadBackupConfig(char * buff, int maxlen) {
	unsigned short FileCRC;
	size_t size = 0;
	if (EEPROMReadArray(ConfigOffset2, &size, sizeof (size_t)) < 0) {
		MyLog("Unable to read config size #1\n");
		return -2;
	}
	if (EEPROMReadArray(ConfigOffset2 + sizeof (size_t), &FileCRC, 2) < 0) {
		MyLog("Unable to read config crc #1\n");
		return -2;
	}
	if (size > MaxConfigLen) {
		return 0;
	}
	if (EEPROMReadArray(ConfigOffset2 + sizeof (size_t) + 2, buff, size) < 0) {
		MyLog("Unable to read config #1\n");
		return -2;
	}
	//Check crc
	if (ComputeChecksum(buff, size) != FileCRC) {
		MyLog("Config #1 CRC mismatch\n");
		return -2;
	}
	return size;
}
#else

int ReadMainConfig(char * buff, int maxlen) {
	int a;
	FILE * SavedConfig = fopen(ConfigPath, "r");
	if (SavedConfig == NULL) {
		return 0;
	} else {
		fseek(SavedConfig, 0, SEEK_END); // seek to end of file
		size_t size = ftell(SavedConfig); // get current file pointer
		fseek(SavedConfig, 0, SEEK_SET); // seek back to beginning of file
		if (size >= maxlen) {
			fclose(SavedConfig);
			return 0;
		}
		size -= 2; //Subtract crc
		unsigned short FileCRC;
		fread(&FileCRC, 1, 2, SavedConfig);
		fread(buff, 1, size, SavedConfig);
		fclose(SavedConfig);
		//Check crc
		if (ComputeChecksum(buff, size) != FileCRC) {
			MyLog("Config #1 CRC mismatch\n");
			return -2;
		}
		return size;
	}
}

int ReadBackupConfig(char * buff, int maxlen) {
	int a;
	FILE * SavedConfig = fopen(BackupConfigPath, "r");
	if (SavedConfig == NULL) {
		return 0;
	} else {
		fseek(SavedConfig, 0, SEEK_END); // seek to end of file
		size_t size = ftell(SavedConfig); // get current file pointer
		fseek(SavedConfig, 0, SEEK_SET); // seek back to beginning of file
		if (size >= maxlen) {
			fclose(SavedConfig);
			return 0;
		}
		size -= 2; //Subtract crc

		unsigned short FileCRC;
		fread(&FileCRC, 1, 2, SavedConfig);
		fread(buff, 1, size, SavedConfig);
		fclose(SavedConfig);
		//Check crc
		if (ComputeChecksum(buff, size) != FileCRC) {
			MyLog("Config #2 CRC mismatch\n");
			//Debug("Config #2 CRC mismatch\n");
			return -2;
		}
		return size;
	}
}
#endif



#ifdef USE_USB_STORAGE

int ReadOnlyBackupConfig(char * buff, int maxlen) {
	int a;
	FILE * SavedConfig = fopen(ReadOnlyConfig, "r");
	if (SavedConfig == NULL) {
		return 0;
	} else {
		fseek(SavedConfig, 0, SEEK_END); // seek to end of file
		size_t size = ftell(SavedConfig); // get current file pointer
		fseek(SavedConfig, 0, SEEK_SET); // seek back to beginning of file
		if (size >= maxlen) {
			fclose(SavedConfig);
			return 0;
		}
		unsigned short FileCRC;
		fread(&FileCRC, 1, 2, SavedConfig);
		fread(buff, 1, size - 2, SavedConfig);
		fclose(SavedConfig);
		//Check crc
		if (ComputeChecksum(buff, size - 2) != FileCRC) {
			MyLog("Read only Config CRC mismatch\n");
			//Debug("Read only Config CRC mismatch\n");
			return -2;
		}
		return size - 2;
	}
}
#endif

void FillConfigInfo(json_value* value) {
	char hb[256];
	char * s;
	int a;
	Info.DeviceName = strdup(json_find_string(value, "Config.Info.DeviceName"));
	/* For test reasons we want to use static definition
	 char * DevId = json_find_string(value, "Config.Info.DeviceSerial");
	a = 0;
	while (*DevId && a < 10) {
		Info.DeviceSerial[a++] = *DevId++;
		if (*DevId == ':') {
			DevId++;
		}
	}*/
	Info.AuthToken = strdup(json_find_string(value, "Config.Info.AuthToken"));
	Info.LastConfigStamp = json_find_int(value, "Config.Info.Unixtime");
	//Player
	PlayerSetup.Ctrl_StreamIn = json_find_int(value, "Config.Player.Ctrl_StreamIn");
	PlayerSetup.Ctrl_RxSelection = json_find_int(value, "Config.Player.CtrlRxSelection");
	//RxStreams
	for (a = 0; a < 32; a++) {
		sprintf(hb, "Config.Player.RxStreams[%i].Name", a);
		PlayerSetup.RxStreams[a].Name = strdup(json_find_string(value, hb));
		sprintf(hb, "Config.Player.RxStreams[%i].Username", a);
		PlayerSetup.RxStreams[a].Username = strdup(json_find_string(value, hb));
		sprintf(hb, "Config.Player.RxStreams[%i].Password", a);
		PlayerSetup.RxStreams[a].Password = strdup(json_find_string(value, hb));
		sprintf(hb, "Config.Player.RxStreams[%i].Url", a);
		PlayerSetup.RxStreams[a].Url = strdup(json_find_string(value, hb));
		sprintf(hb, "Config.Player.RxStreams[%i].Encoding", a);
		PlayerSetup.RxStreams[a].Encoding = strcmp(json_find_string(value, hb), "AAC") == 0 ? EncType_AAC : EncType_MP3;
		//sprintf(hb, "Config.Player.RxStreams[%i].Active", a);
		PlayerSetup.RxStreams[a].Active = strlen(PlayerSetup.RxStreams[a].Url) > 0;
	}
	//Stream
	StreamSetup.ControlType = json_find_int(value, "Config.Stream.ControlType");
	StreamSetup.BitRate = json_find_int(value, "Config.Stream.BitRate");
	StreamSetup.EncType = json_find_int(value, "Config.Stream.EncType");
	StreamSetup.StreamOnLevel = json_find_int(value, "Config.Stream.StreamOnLevel");
	StreamSetup.StreamOnDelay = json_find_int(value, "Config.Stream.StreamOnDelay");
	//Mounts[2]
	StreamSetup.Mounts[0].SourceName = strdup(json_find_string(value, "Config.Stream.Mounts[0].SourceName"));
	StreamSetup.Mounts[0].UserName = strdup(json_find_string(value, "Config.Stream.Mounts[0].UserName"));
	StreamSetup.Mounts[0].Password = strdup(json_find_string(value, "Config.Stream.Mounts[0].Password"));
	StreamSetup.Mounts[0].Name = strdup(json_find_string(value, "Config.Stream.Mounts[0].Name"));
	StreamSetup.Mounts[0].URL = strdup(json_find_string(value, "Config.Stream.Mounts[0].URL"));
	StreamSetup.Mounts[0].Port = json_find_int(value, "Config.Stream.Mounts[0].Port");
	StreamSetup.Mounts[1].SourceName = strdup(json_find_string(value, "Config.Stream.Mounts[1].SourceName"));
	StreamSetup.Mounts[1].UserName = strdup(json_find_string(value, "Config.Stream.Mounts[1].UserName"));
	StreamSetup.Mounts[1].Password = strdup(json_find_string(value, "Config.Stream.Mounts[1].Password"));
	StreamSetup.Mounts[1].Name = strdup(json_find_string(value, "Config.Stream.Mounts[1].Name"));
	StreamSetup.Mounts[1].URL = strdup(json_find_string(value, "Config.Stream.Mounts[1].URL"));
	StreamSetup.Mounts[1].Port = json_find_int(value, "Config.Stream.Mounts[1].Port");

	//Firmware
	SysServices.UpdateUrl = strdup(json_find_string(value, "Config.Services.UpdateUrl"));
	SysServices.DoUpdate = json_find_int(value, "Config.Services.DoUpdate");
	SysServices.ControlUrl = strdup(json_find_string(value, "Config.Services.ControlUrl"));
	SysServices.RemoteControl = json_find_int(value, "Config.Services.RemoteControl");
	SysServices.PreAuthToken = strdup(json_find_string(value, "Config.Services.PreAuthToken"));
	SysServices.PollPeriod = json_find_int(value, "Config.Services.PollPeriod");


	//Hardware
	HardwareSetup.HardwareGain = json_find_int(value, "Config.Hardware.HardwareGain");
	HardwareSetup.SampleRate = json_find_int(value, "Config.Hardware.SampleRate");
	HardwareSetup.InVolume = json_find_double(value, "Config.Hardware.InVolume");
	HardwareSetup.OutVolume = json_find_int(value, "Config.Hardware.OutVolume");
	HardwareSetup.AutoLimiter = json_find_int(value, "Config.Hardware.AutoLimiter");
	//DSP
	char * SymIp = json_find_string(value, "Config.DSP.IP");
	char * pch = strtok(SymIp, ".");
	if (pch == NULL) {
		MyLog("Config error at Symnet IP#1\n");
		goto Skip;
	}
	DspSettings.IP.v[0] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		MyLog("Config error at Symnet IP#2\n");
		goto Skip;
	}
	DspSettings.IP.v[1] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		MyLog("Config error at Symnet IP#3\n");
		goto Skip;
	}
	DspSettings.IP.v[2] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		MyLog("Config error at Symnet IP#4\n");
		goto Skip;
	}
	DspSettings.IP.v[3] = strtol(pch, NULL, 10);
Skip:
	DspSettings.Port = json_find_int(value, "Config.DSP.Port");
	DspSettings.TCP = json_find_int(value, "Config.DSP.TCP");
	DspSettings.Stream1 = json_find_int(value, "Config.DSP.Stream1");
	DspSettings.Stream2 = json_find_int(value, "Config.DSP.Stream2");
	DspSettings.StreamIn = json_find_int(value, "Config.DSP.StreamIn");
	DspSettings.AudioLevel = json_find_int(value, "Config.DSP.AudioLevel");
	DspSettings.IsStreaming1 = json_find_int(value, "Config.DSP.IsStreaming1");
	DspSettings.IsStreaming2 = json_find_int(value, "Config.DSP.IsStreaming2");
	DspSettings.AudioPeak = json_find_int(value, "Config.DSP.AudioPeak");
	DspSettings.RxSelection = json_find_int(value, "Config.DSP.RxSelection");
}

int SaveNewConfig(void) {
	int a;
	MyLog("Creating new configuration file\n");
	//Load some default strings    
	PlayerSetup.RxStreams[0].Active = 1;
	PlayerSetup.RxStreams[0].Name = strdup("Aleph test");
	PlayerSetup.RxStreams[0].Url = strdup("http://icecast.aleph-com.net:8000/9235230.mp3");
	PlayerSetup.RxStreams[0].Password = strdup("");
	PlayerSetup.RxStreams[0].Username = strdup("");
	for (a = 1; a < 32; a++) {
		PlayerSetup.RxStreams[a].Active = 0;
		PlayerSetup.RxStreams[a].Name = strdup("");
		PlayerSetup.RxStreams[a].Url = strdup("");
		PlayerSetup.RxStreams[a].Password = strdup("");
		PlayerSetup.RxStreams[a].Username = strdup("");
	}
	HardwareSetup.SampleRate = 1;
	StreamSetup.Mounts[0].SourceName = strdup("friesen.mp2");
	StreamSetup.Mounts[0].UserName = strdup("source");
	StreamSetup.Mounts[0].Password = strdup("hackme");
	StreamSetup.Mounts[0].Name = strdup("8442740");
	StreamSetup.Mounts[0].URL = strdup("192.168.0.100");
	StreamSetup.Mounts[1].SourceName = strdup("friesen.mp2");
	StreamSetup.Mounts[1].UserName = strdup("source");
	StreamSetup.Mounts[1].Password = strdup("hackme");
	StreamSetup.Mounts[1].Name = strdup("8442741");
	StreamSetup.Mounts[1].URL = strdup("192.168.0.100");

	Info.DeviceName = strdup("New Device");
	Info.AuthToken = strdup("");
	SysServices.UpdateUrl = strdup("http://easylisten.net/firmware.bin");
	SysServices.ControlUrl = strdup("http://easylisten.net/control.php");
	SysServices.RemoteControl = 1;

	return SaveCurrentConfig();
}
