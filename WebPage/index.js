/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Data_StaticConfig = new Record("io/config.json", "json", true);
var Data_Config = new Record("io/config.json", "json", false);
var Data_Info = new Record("io/info.json", "json", true);
var DownLoad = new Ajax_class(ShowProgress, 10000);
var DataStream;
var LastChangedID = -1;

Math.log10 = function (x) { return Math.log(x) / Math.LN10; };

DownLoad.GetItems([Data_StaticConfig], function (valid) {
	if (valid == true) {
        FillConfigs();
		GetInfo();
	} else {
		//console.log("Error");
	}
});

function ShowProgress() {

}

var StreamStarted = false;
var InfoStream = false;
var GetInfoTimer;
var PendingChanges = false;

function StartStream() {
	if (!StreamStarted) {
		StreamStarted = true;
		slctid('StartStop').innerHTML = "Stop scope";
		StreamAjax();
	} else {
		StreamStarted = false;
		slctid('StartStop').innerHTML = "Start scope";
	}
}

function StopStream() {
	StreamStarted = false;
	slctid('StartStop').innerHTML = "Start scope";
}

StreamAjax = function () {
	var x = new XMLHttpRequest();
	x.open("GET", 'stream.raw?' + Date.now(), true);
	x.timeout = 5000;
	x.ontimeout = function () {

	};
	x.onreadystatechange = function ()
	{
		//ObjectRef.progressvalue = x.readyState;
		//This.ShowProgressHandler();
		if (x.readyState == 4)
		{
			if (x.responseText == "") {
				if (StreamStarted) {
					setTimeout(StreamAjax, 5000);
				}
			} else {
				FillScope(x.responseText);
				if (StreamStarted) {
					setTimeout(StreamAjax, 100);
				}
			}
		}

	};
	x.send(null);
};

function GetInfo() {
	clearTimeout(GetInfoTimer);
    Data_Config.ReloadRequired = true;
    Data_Config.GetParams = "date=" + Date.now();
    DownLoad.GetItems([Data_Info], function (valid) {
        if (valid == true) {
            var Info = Data_Info.data.Info;
			slctid('Stream1Connected').innerHTML = Info.Streams[0].Status;
            slctid('Stream2Connected').innerHTML = Info.Streams[1].Status;
			slctid('ListenStatus').innerHTML = Info.PlayState;

            var ProgressScale = (parseFloat(Info.AudioPeak) + 30) / .48;
            if (Info.AudioPeak > 17.0) {
                slctid('AudioPeak').className = "progress-bar progress-bar-danger";
			} else if (Info.AudioPeak > 15.0) {
                slctid('AudioPeak').className = "progress-bar progress-bar-warning";
			} else {
                slctid('AudioPeak').className = "progress-bar progress-bar-success";
			}
            slctid('AudioPeak').style = 'width:' + ProgressScale + '%';
            
            ProgressScale = (parseFloat(Info.AudioLevel) + 30) / .48;
            slctid('AudioLevel').style = 'width:' + ProgressScale + '%';
            if (Info.AudioLevel > 15.0) {
                slctid('AudioLevel').className = "progress-bar progress-bar-danger";
            } else if (Info.AudioLevel > 13.0) {
                slctid('AudioLevel').className = "progress-bar progress-bar-warning";
            } else {
                slctid('AudioLevel').className = "progress-bar progress-bar-success";
            }

            slctid('BuffInMs').innerHTML = (parseFloat(Info.PlayerBuffer) / 1000).toFixed(1);
            if (PendingChanges == false) {
                if (Info.Streams[0].Connected == 1) {
                    slctid('Stream1ControlButton').innerHTML = "Stop Stream";
                } else if (Info.Streams[0].CtrlStream == 1) {
                    slctid('Stream1ControlButton').innerHTML = "Stop Stream Connect";
                } else {
                    slctid('Stream1ControlButton').innerHTML = "Start Stream";
                }
                if (Info.Streams[1].Connected == 1) {
					slctid('Stream2ControlButton').innerHTML = "Stop Stream";
                } else if (Info.Streams[1].CtrlStream == 1) {
                    slctid('Stream2ControlButton').innerHTML = "Stop Stream Connect";
                } else {
                    slctid('Stream2ControlButton').innerHTML = "Start Stream";
                }
                if (Info.CtrlStreamIn == 1) {
					slctid('PlayerControlButton').innerHTML = "Stop Player";
				} else {
					slctid('PlayerControlButton').innerHTML = "Start Player";
				}
			}

			var VAL = "None";
			switch(parseInt(Info.DSPError)){
					//SE_NONE, SE_SOCKERR, SE_NOCONN, SE_SORCV, SE_SOSND, SE_SQEH, SE_NORESP, SE_NORECV, SE_NOACT
					case 0: VAL = 'None';break;
					case 1: VAL = 'Unable to open';break;
					case 2: VAL = 'Unable to connect';break;
					case 3: VAL = 'SO_RCV err';break;
					case 4: VAL = 'SO_SND err';break;
					case 5: VAL = 'SQ EH err';break;
					case 6: VAL = 'No DSP ack';break;
					case 7: VAL = 'No DSP response';break;
					case 8: VAL = 'No DSP activity';
					break;
			}
			slctid('SymSettings_Error').innerHTML = VAL;
			slctid('versioninfo').innerHTML = Info.Version;
			slctid('ControlStatus').innerHTML = Info.ControlStatus;
			if (Info.DeviceName == "") {
				slctid('nameinfo').innerHTML = '-Not named-';
			} else {
				slctid('nameinfo').innerHTML = Info.DeviceName;
            }
            if (Info.EncType == 0) {
                $('.MP3').show();
                $('.AAC').hide();
            } else {
                $('.AAC').show();
                $('.MP3').hide();
            }
            if (LastChangedID != Info.Unixtime) {
                DownLoad.GetItems([Data_StaticConfig], function (valid) {
                    if (valid == true) {
                        FillConfigs();
                    } else {
                        //console.log("Error");
                    }
                });
            }
            GetInfoTimer = setTimeout(GetInfo, 500);
        } else {
            GetInfoTimer = setTimeout(GetInfo, 10000);
        }
    });
}

//function StartInfo(){
//	setTimeout(GetInfo, 2000);
//}

function FillScope(data) {
	try{
		data = atob(data);
	} catch (e){
		return;
	}
	try {
		var c = document.getElementById("myCanvas");
		var ctx = c.getContext("2d");
		ctx.fillStyle = "#DDD";
		ctx.fillRect(0, 0, 441, 256);
		ctx.lineWidth = 0.5;
		ctx.strokeStyle = "#00A";
		var LastX = 0;
		var LastY = ConvertByte(data.charCodeAt(0)) / 2 + 64;

		var ThisX = 0;
		var ThisY = 0;
		for (var b = 1; b < data.length; b++) {
			LastX = ThisX;
			LastY = ThisY;
			ThisX = b / 2.5;
			ThisY = ConvertByte(data.charCodeAt(b)) / 2 + 64;
			ctx.beginPath();
			ctx.moveTo(ThisX, ThisY);
			ctx.lineTo(LastX, LastY);
			//ctx.strokeStyle = "#00F";
			ctx.stroke();
			ctx.closePath();
			ctx.beginPath();
			//ctx.arc(LastX, LastY, 0.6, 0, 2 * Math.PI, false);
			ctx.fillStyle = "#0A0";
			ctx.fill();
			ctx.stroke();
			ctx.closePath();
		}
	} catch (er) {

	}
}

function ConvertByte(val) {
	if (val > 127) {
		val -= 256;
	}
	return val;
}

function FillConfigs() {
    var config = Data_StaticConfig.data.Config;
    var E = 'edit_';
    //delete config.AudioSetup.AvailableList;
    LastChangedID = config.Info.Unixtime;	
                
    var StreamEnums = '';
    var playtable = '<tr><td>Player setup</td><td></td><td></td></tr>';
    for (var a = 0; a < 32; a++) {
        playtable += '<tr><td>' + (a + 1) + '</td>';
        var name = config.Player.RxStreams[a].Name;
        if(name == undefined || name == ''){
            name = '---';
        }
        playtable += '<td>' + name + '</td>';
        playtable += '<td><button class="btn btn-info" style="width:100%;padding:0px;" onclick="EditStream(' + a + ')">Edit</button></td></tr>';
        if (config.Player.RxStreams[a].Url != '' && config.Player.RxStreams[a].Url != undefined) {
            StreamEnums += '<option data-enum="' + (a + 1) + '">' + config.Player.RxStreams[a].Name + '</option>';
        }
    }
    slctid('edit_Player_CtrlRxSelection').innerHTML = StreamEnums;
    slctid('playsetuptable').innerHTML = playtable;
    slctid('FirmwareVersion').innerHTML = config.Info.Version;
    var info_DeviceName = config.Info.DeviceName;
	 var info_AuthToken = config.Info.AuthToken;
    delete config.Info;
    config.Info = {};
    config.Info.DeviceName = info_DeviceName;
	 config.Info.AuthToken = info_AuthToken;
    for (var key in config) {
        if (typeof config[key] === 'object') {
            for (var subkey in config[key]) {
				if (typeof config[key][subkey] !== 'object') {
					SetInputItemValue("edit_" + key + "_" + subkey, config[key][subkey], function (value) {
						if (config == null) {
							return;
						}
						var Values = value.id.split('_');
						var TopObj = Values[1];
						var BotObj = Values[2];
						if (Values.length > 3) {
							BotObj += '_' + Values[3];
						}
						config[TopObj][BotObj] = GetInputItemValue(value.id);
						UpdateChanges(value.id);
                    });
                } else {
                    if(Array.isArray(config[key][subkey])){
                        var len = config[key][subkey].length;
                        var ar = config[key][subkey];
                        for (var i = 0; i < len; i++) {
                            for (var sssk in ar[i]) {
                                var editline = 'edit_' + key + '_' + subkey + '_' + i + '_' + sssk;
                                SetInputItemValue(editline, ar[i][sssk], function (value) {
                                    if (config == null) {
                                        return;
                                    }
                                    var Values = value.id.split('_');
                                    var TopObj = Values[1];
                                    var BotObj = Values[2];
                                    var ArrObj = Values[3];
                                    var EndObj = Values[4];
                                    config[TopObj][BotObj][ArrObj][EndObj] = GetInputItemValue(value.id);
                                    UpdateChanges(value.id);
                                });
                            }
                        }
                    }
                }
            }
		}
	}
	addOnclickById('Stream1ControlButton', function () {
		PendingChanges = true;
		if (config.Stream.Mounts[0].CtrlStream == 1) {
			config.Stream.Mounts[0].CtrlStream = 0;
			slctid('Stream1ControlButton').innerHTML = "Start Stream";
		} else {
			config.Stream.Mounts[0].CtrlStream = 1;
			slctid('Stream1ControlButton').innerHTML = "Stop Stream";
		}
		UpdateChanges('Stream1ControlButton');
	});
	addOnclickById('Stream2ControlButton', function () {
		PendingChanges = true;
		if (config.Stream.Mounts[1].CtrlStream == 1) {
			config.Stream.Mounts[1].CtrlStream = 0;
			slctid('Stream2ControlButton').innerHTML = "Start Stream";
		} else {
			config.Stream.Mounts[1].CtrlStream = 1;
			slctid('Stream2ControlButton').innerHTML = "Stop Stream";
		}
		UpdateChanges('Stream2ControlButton');
	});
	addOnclickById('PlayerControlButton', function () {
		PendingChanges = true;
		if (config.Player.CtrlStreamIn == 1) {
			config.Player.CtrlStreamIn = 0;
			slctid('PlayerControlButton').innerHTML = "Start Player";
		} else {
			config.Player.CtrlStreamIn = 1;
			slctid('PlayerControlButton').innerHTML = "Stop Player";
		}
		UpdateChanges('PlayerControlButton');
	});
	slctid('nameinfo').onclick = function (e){
		if(e.ctrlKey){
			slctid('Tab7').removeAttribute('style');
		}
    };
}

function RenameDevice() {
    if(Data_StaticConfig.data.Config.Info == undefined){
        Data_StaticConfig.data.Config.Info = {};
    }
	Data_StaticConfig.data.Config.Info.RenameUnlock = 'true';
	UpdateChanges('RenameDevice');
	slctid('Tab7').setAttribute('style', 'display:none;');
	$('.TabD').hide();
	$('#Tab0Display').show();
}

function NewAuthToken() {
    if(Data_StaticConfig.data.Config.Info == undefined){
        Data_StaticConfig.data.Config.Info = {};
    }
	Data_StaticConfig.data.Config.Info.AuthTokenUnlock = 'true';
	UpdateChanges('NewAuthToken');
	slctid('Tab7').setAttribute('style', 'display:none;');
	$('.TabD').hide();
	$('#Tab0Display').show();
}

function StartUpdate() {
    if (Data_StaticConfig.data.Config.Info == undefined) {
        Data_StaticConfig.data.Config.Info = {};
    }
    Data_StaticConfig.data.Config.Firmware.DoUpdate = 1;
    UpdateChanges('StartUpdate');
}

var ChangeTimer;
var UpdatePlayList = false;
function UpdateChanges(caller) {
    clearTimeout(ChangeTimer);
	 if(caller == 'EditStream'){
		 UpdatePlayList = true;
	 }
    ChangeTimer = setTimeout(function () {
        if (Data_StaticConfig.data.Config.Info == undefined) {
            Data_StaticConfig.data.Config.Info = {};
        }
        Data_StaticConfig.data.Config.Info.Unixtime = Math.round((new Date()).getTime() / 1000);
        LastChangedID = Data_StaticConfig.data.Config.Info.Unixtime;
        AjaxPost("POST", "io/config.json?date=" + Date.now(), Data_StaticConfig.data, true, function (x) {
            if (x.status == 200) {
                var NewConfig = JSON.parse(x.responseText);
                if (Data_StaticConfig.data.Config.Info == undefined) {
                    Data_StaticConfig.data.Config.Info = {};
                }
                Data_StaticConfig.data.Config.Info.RenameUnlock = 'false';
					 Data_StaticConfig.data.Config.Info.AuthTokenUnlock = 'false';
					 if(UpdatePlayList == true){
						 Data_StaticConfig.data = NewConfig;
						 FillConfigs();
						 UpdatePlayList = false;
					 }
            } else {
                showHtmlAlert(x.responseText, "alert-danger");
			}
			PendingChanges = false;
		});
	}, 2500);
}

function AjaxPost(type, url, data, sync, callback)
{
	//console.log("JSONPOST", data);
	var x = new XMLHttpRequest();
	x.open(type, url, sync);
	x.timeout = 10000;
	x.ontimeout = function () {
		var reply = {};
		reply.status = "timeout";
		reply.readyState = 4;
		callback(reply);
	};
	x.onreadystatechange = function ()
	{
		if (x.readyState == 4 && x.status == 200)
		{
			callback(x);
		}
		else if (x.readyState == 4)
		{
			var error = json(x.responseText);
			//console.log("ERROR", error);
			callback(x);
		}
    };
    x.send(JSON.stringify(data));
}
//function showModalWithButtons(heading, messageHtml, buttons, InfoObject, callback, CustomAction, EditEvent) 
ph = '';
ph += '<table class = "table">';
ph += '	<tr>';
ph += '		<td>Name</td>';
ph += '		<td><input type="text" class="form-control" id="edit_Name" placeholder="Name" maxlength="32"></td>';
ph += '	</tr>';
ph += '	<tr>';
ph += '		<td>Username</td>';
ph += '		<td><input type="text" class="form-control" id="edit_Username" placeholder="Username" maxlength="64"></td>';
ph += '	</tr>';
ph += '	<tr>';
ph += '		<td>Password</td>';
ph += '		<td><input type="text" class="form-control" id="edit_Password" placeholder="Password" maxlength="64"></td>';
ph += '	</tr>';
ph += '	<tr>';
ph += '		<td>Url</td>';
ph += '		<td><input type="text" class="form-control" id="edit_Url" placeholder="Url" maxlength="64"></td>';
ph += '	</tr>';
ph += '	<tr>';
ph += '		<td>Encoding</td>';
ph += '		<td><select data-isenum="true" class="form-control" id="edit_Encoding"><option>MP3</option><option data-enum="1">AAC</option></select></td>';
ph += '	</tr>';
ph += '<table>';
var ModalHtml = ph;

function EditStream(index) {
    var Stream = ObjectCopy(Data_StaticConfig.data.Config.Player.RxStreams[index]);
    showModalWithButtons("Edit Stream " + index, ModalHtml, [
        {clickResult: "cancel", caption: "Cancel", cssClasses: "btn btn-primary"},
        {clickResult: "submit", caption: "Submit", cssClasses: "btn btn-primary"}
    ], Stream, function (result, modal) {
        //callback
        if(result == 'submit'){
            Data_StaticConfig.data.Config.Player.RxStreams[index] = Stream;
            UpdateChanges('EditStream');      
        }
        modal.modal("hide");
    }, function(){
        //CustomAction
    });
}