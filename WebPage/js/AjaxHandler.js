/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function Record(URL, Type, AlwaysReload) {
	var This = this;
	This.AjaxInProgress = false;
	This.ReloadRequired = true;
	if(AlwaysReload == null){
		This.AlwaysReload = false;
	} else {
		This.AlwaysReload = AlwaysReload;
	}
	This.url = URL;
	This.data = null;
	This.id = null;
	This.idname = null;
	This.type = Type;
	This.progressvalue = 0;
	This.GetParams = null;
	This.DoPost = false;
	This.valid = function () {
		return This.data != null;
	};
	This.SetId = function (Id, IdName) {
		This.id = Id;
		This.idname = IdName;
		This.data = null;
	};
	This.GetUrl = function () {
		if (This.id == null) {
			if (This.GetParams == null) {
				return This.url;
			} else {
				var RetVal = This.url + "?" + This.GetParams;
				This.GetParams = null;//Single use
				return RetVal;
			}
		} else {
			if (This.GetParams == null) {
				return This.url + "?" + This.idname + "=" + This.id;
			} else {
				var RetVal = This.url + "?" + This.idname + "=" + This.id + "&" + This.GetParams;
				This.GetParams = null;//Single use
				return RetVal;
			}
		}
	};
	This.AjaxType = function () {
		if(This.DoPost == true){
			This.DoPost = false;
			return "POST";
		}else {
			return "GET";
		}
	};
}

function Ajax_class(ProgressCallback, Timeout) {
	var This = this;
	This.InProgress = false;
	This.OnFinish = null;
	This.OnProgress = ProgressCallback;
	This.TotalItems = 0;
	This.ProgressItems = 0;
	This.Errors = false;
	This.Timeout = Timeout;
	This.CurrentArray = null;
	This.GetItems = function (array, On_Finish) {
		if (This.InProgress == true) {
			This.OnProgress("done");
			On_Finish({"error": "Another call in progress."});
			return;
		}
		This.OnFinish = On_Finish;
		This.InProgress = true;
		This.ProgressItems = This.TotalItems = array.length;
		This.Errors = false;
		This.CurrentArray = array;
		var AjaxThisTime = false;
		for (var a = 0; a < array.length; a++) {
			var ObjRef = array[a];
			//Limit calls if data is already valid
			if(ObjRef.ReloadRequired || !ObjRef.valid() || ObjRef.AlwaysReload){
				This.Ajax(ObjRef);
				AjaxThisTime = true;
				ObjRef.progressvalue = 0;
			} else {
				ObjRef.progressvalue = 4;
				This.TotalItems--;
			}
		}
		if (AjaxThisTime == false) {
			This.OnProgress("stop");
			This.InProgress = false;
			This.OnFinish(true);
		} else {
			This.OnProgress("start");
		}
	};
	This.DataHandler = function (Response, ObjectRef) {
		This.TotalItems--;
		if (Response.status == 200) {
			ObjectRef.AjaxInProgress = false;
			if(ObjectRef.type == "json"){
				ObjectRef.data = json(Response.responseText);
			} else {
				ObjectRef.data = Response.responseText;
			}
			ObjectRef.ReloadRequired = false;
		} else if(Response.status == 403){
			window.location.href = "LoginHandler.php";
		} else {
			ObjectRef.ReloadRequired = true;
			ObjectRef.data = null;
			This.Errors = true;
			//console.log(ObjectRef.url);
			//console.log(json(Response.responseText));
			//console.log(Response.status);
		}
		if (This.TotalItems == 0) {
			This.OnProgress("stop");
			This.InProgress = false;
			if (This.Errors == true) {
				This.OnFinish(false);
			} else {
				This.OnFinish(true);
			}
		}
	};
	This.Ajax = function (ObjectRef) {
		var x = new XMLHttpRequest();
		var AjaxRef = ObjectRef;
		x.open(ObjectRef.AjaxType(), ObjectRef.GetUrl(), true);
		x.timeout = This.Timeout;
		x.ontimeout = function () {
			var reply = {};
			reply.status = "timeout";
			reply.readyState = 4;
			This.DataHandler(reply, AjaxRef);
		};
		x.onreadystatechange = function ()
		{
			ObjectRef.progressvalue = x.readyState;
			This.ShowProgressHandler();
			if (x.readyState == 4)
			{
				This.DataHandler(x, AjaxRef);
			}
		};
		x.send(null);
	};
	This.ShowProgressHandler = function () {
		var Total = 0;
		var Status = 0;
		for (var a = 0; a < This.CurrentArray.length; a++) {
			Status += This.CurrentArray[a].progressvalue;
			Total += 4;
		}
		This.OnProgress(100 * Status / Total);
		//console.log("Total " + Total + " Status " + Status + " " + (100 * Status / Total) + "%");
	};
}

function json(data) {
	try {
		return JSON.parse(data);
	} catch (error) {
		//console.log(error);
		//console.log(data);
	}
}


