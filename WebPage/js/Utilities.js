function Ajax(type, url, callback)
{
	var x = new XMLHttpRequest();
	x.open(type, url, true);
	x.timeout = 7500;
	x.ontimeout = function () {
		var reply = {};
		reply.status = "timeout";
		reply.readyState = 4;
		callback(reply);
	};
	x.onreadystatechange = function ()
	{
		if (x.readyState == 4)
		{
			callback(x);
		}
	};
	x.send(null);
}

function json(data) {
	try {
		return JSON.parse(data);
	} catch (error) {
		//console.log(error);
		//console.log(data);
	}
}

function SetInputItemOnChange(id, func) {
	try {
		var element = document.getElementById(id);
		element.onchange = function () {
			func(element);
		};
	} catch (err) {
		//console.log(id + " doesn't exist.");
	}
}

function SetInputItemValue(id, value, func) {
	try {
		var element = document.getElementById(id);
		var type = element.type;
		if(typeof type == "undefined"){
			type = document.querySelector('#' + id).dataset.type;
		}
		switch (type) {
			case "checkbox":
				if (value == "1") {
					element.checked = true;
				} else {
					element.checked = false;
				}
				break;
			case "number":
				try {
					var UpElem = document.getElementById(id + "--p");
					var DownElem = document.getElementById(id + "--m");
					UpElem.setAttribute('onclick', 'NumericUpDown(this)');
					DownElem.setAttribute('onclick', 'NumericUpDown(this)');
				} catch (er) {
					//Wasn't what we thought.
				}
			case "select-one":
				var att = element.getAttribute("data-isenum");
				if(att != null && att == "true") {
					var Options = element.childNodes;
					for (var a = 0; a < Options.length; a++) {
						if (Options[a].value != undefined) {
							if (value == Options[a].getAttribute("data-enum")) {
								element.value = Options[a].innerHTML;
							}
						}
					}
				} else {
					element.value = value;
				}
				break;
			case "text":
			case "time":
			case "email":
			case "tel":
				element.value = value;
				break;
			case "date":
				if (value < 946771200) {
					value = 946771200;
				}
				element.value = moment(value * 1000).format('YYYY-MM-DD');
				break;
			case "datetime-local":
				if (value < 946771200) {
					value = 946771200;
				}
				element.value = moment(value * 1000).format('YYYY-MM-DD[T]HH:mm');
				break;
			case "JQdate":
				$('#' + id).datetimepicker({
					viewMode: 'years',
					format: 'MM/DD/YYYY',
					pickTime: false
				});
				var dateTimePicker = $('#' + id);
				var data = dateTimePicker.data("DateTimePicker");
				if (!data) {
					dateTimePicker.datetimepicker();
					data = dateTimePicker.data("DateTimePicker");
				}
				if (isNaN(value)) {
					throw new TypeError("JQdatetime expects a number or number string.  Got a value of " + value);
				} else {
					value = value * 1000;
				}
				if (value < 946771200000) {
					value = 946771200000;
				}
				data.setDate(moment(value));
				break;
			case "JQdatetime":
				$('#' + id).datetimepicker();
				var dateTimePicker = $('#' + id);
				var data = dateTimePicker.data("DateTimePicker");
				if(!data) {
					dateTimePicker.datetimepicker();
					data = dateTimePicker.data("DateTimePicker");
				}
				if(isNaN(value)) {
					throw new TypeError("JQdatetime expects a number or number string.  Got a value of " + value);
				} else {
					value = value * 1000;
				}
				data.setDate(moment(value));
				break;
			default:
				element.innerHTML = value;
		}
		if (func != undefined) {
			element.onchange = function () {
				func(element);
			};
		}
	} catch (er) {
		//alert(er + "\r\nAt Location " + id);
	}
}

function GetInputItemValue(id){
	try{
		var element = document.getElementById(id);
		var type = element.type;
		if (typeof type == "undefined") {
			type = document.querySelector('#' + id).dataset.type;
		}
		switch (type) {
			case "checkbox":
				if(element.checked){
					return "1";
				} else {
					return "0";
				}
				break;
			case "select-one":
				var att = element.getAttribute("data-isenum");
				if (att != null && att == "true") {
					var Options = element.childNodes;
					for (var a = 0; a < Options.length; a++) {
						if (element.value == Options[a].innerHTML) {
							return Options[a].getAttribute("data-enum");
						}
					}
					return null;
				} else {
					element.value = value;
				}
				break;
			case "number":
			case "text":
			case "time":
			case "email":
			case "tel":
			case "password":
			case "textarea":
				return element.value;
			case "JQdate":
			case "JQdatetime":
				return $('#' + id).data("DateTimePicker").getUTC();
				break;
			case "date":
			case "datetime-local":
				return moment(element.value).unix();
				break;
			default:
				return element.innerHTML;
		}
	} catch(er){
		alert(er + "\r\nInput error at Location " + id);
	}
}

function FillFormSelection(id, info, item, filter, UseEnum) {
	if (typeof item == "undefined" || item == null) {
		item = 'item';
	}
	if (typeof filter == "undefined" || filter == null) {
		filter = false;
	}
	if (typeof info == "undefined" || info == null) {
		return;
	}
	if (typeof UseEnum == "undefined") {
		UseEnum = false;
	}
	try {
		var element = document.getElementById(id);
		element.innerHTML = "";
		for (var a = 0; a < info.length; a++) {
			if (filter === false || info[a][filter.item] == filter.match) {
				var Option = document.createElement('option');
				Option.innerHTML = info[a][item];
				if (UseEnum != false) {
					Option.setAttribute("data-enum", info[a][UseEnum]);
				}
				element.appendChild(Option);
			}
		}
	} catch (er) {
		alert(er + "\r\nLocation: " + info + " id:" + id);
	}
}

function AddFormSelection(id, item) {
	var element = document.getElementById(id);
	var Option = document.createElement('option');
	Option.innerHTML = item;
	element.appendChild(Option);
}

function BoolToString(value) {
	if (value == '1') {
		return "Yes";
	} else {
		return "No";
	}
}

/*
 * Helper function for IOS
 */
function LimitMinMax(item) {
	try {
		var element = item;//document.getElementById(item.id);
		if(element.type != "number"){
			return;
		}
		var max = Number(element.max);
		var min = Number(element.min);
		var OldValue = Number(element.value);
		if (OldValue > max) {
			element.value = max;
		} else if (OldValue < min) {
			element.value = min;
		}
		switch (element.step){
			case "0.01":
			case "0.25":
				element.value = parseFloat(element.value).toFixed(2);
				break;
		}
	} catch (err) {
		alert(err + "\r\nAt Location " + item);
	}
}

function JsonPost(type, url, data, sync, callback, showprogress)
{
	if(showprogress == true){
		ShowProgress("start");
	}
	//console.log("JSONPOST", data);
	var x = new XMLHttpRequest();
	x.open(type, url, sync);
	if (type == 'POST') {
		x.setRequestHeader("Content-type", "application/json");
	}
	x.timeout = 15000;
	x.ontimeout = function () {
		var reply = {};
		reply.status = "timeout";
		reply.readyState = 4;
		callback(reply);
	};
	x.onreadystatechange = function ()
	{
		ShowProgress(x.readyState * 25);
		if (x.readyState == 4 && x.status == 200)
		{
			if (showprogress == true) {
				ShowProgress("stop");
			}
			callback(x);
		}
		else if (x.readyState == 4)
		{
			if (showprogress == true) {
				ShowProgress("stop");
			}
			if (x.status == 403){
				window.location.href = "LoginHandler.php";
			}
			var error = json(x.responseText);
			var message = "";
			//console.log("ERROR", error);
			callback(x);
		}
	};
	x.send(JSON.stringify(data));
	//console.log(JSON.stringify(data));
}

function cloneChildren (master, target_div) {

	if (target_div == null) { // make a new div to return if none is given
		var target_div = document.createElement("div");
	}
	var children = master.childNodes;
	for (var x=0;x<children.length;x++) {
		
		var copy = children[x].cloneNode(true);
		target_div.appendChild(copy);
	}

	if (target_div == null) { // if no target div was given return new one
		return div;
	}
}

function addOnclick (el, func, param) {
	el.onclick = function () {func(param);};
}

function addOnclickById (id, func, param) {
	try{
		document.getElementById(id).onclick = function () {func(param);};
	} catch(err){
		//console.log(err + " at " + id);
		throw(err);
	}
}

function StInrH(id, value) {
	document.getElementById(id).innerHTML = value;
}

function RemoveById(id){
	document.getElementById(id).parentNode.innerHTML = "";
}

function SetDisplayStyle(id, value) {
	document.getElementById(id).style.display = value;
}
			
function StringContains(String,Value){
	if(String == null){
		return false;
	}
	if(Value == "" && Value != String){
		return false;
	}
	if(String.indexOf(Value) > -1){
		return true;
	} else {
		return false;
	}
}

function ObjectCopy(object){
	return JSON.parse(JSON.stringify(object));
}

function slctid(string) {
//	return slct("id", string);
	return document.getElementById(string);
}
// My own little utility function for templating.
// Could be greatly improved, but don't want to waste time on it right now.
function resolveTemplate(template, vars, processFn) {
	var str = template;
	
	if(processFn) {
		vars = processFn(vars);
	}

	for(var i in vars) {
		str = str.replace(new RegExp(escapeRegExp("{{" + i + "}}"), 'g'), vars[i]);
	}

	return str;
}

function showHtmlAlert(message, cssClass, displaydiv) {
	var newAlert = $('<div class="alert" role="alert"></div>');
	newAlert.addClass(cssClass);

	var alertMessage = $("<span></span>");
	var closeButton = $('<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>');

	alertMessage.html(message);

	newAlert.append(alertMessage);
	newAlert.append(closeButton);
	if (displaydiv == undefined) {
		slctid("showalert_div").removeAttribute("style");
		$("#showalert_div").append(newAlert);
	} else {
		slctid(displaydiv).removeAttribute("style");
		$("#" + displaydiv).append(newAlert);
	}
	newAlert.alert();
}

function showHtmlAlertWithButtons(heading, message, cssClass, buttons, callback) {
	heading = heading || "Alert";
	var modalOuter = $("#myModal");
	$("#myModalLabel").html(heading);
	var alertMessage = $("<p></p>");	
	alertMessage.html(message);
	$("#MyModalBody").html(alertMessage);
	var buttonsDiv = $("<div></div>");
	$("#MyModalAlert").html("");
	if(typeof buttons == 'undefined') {
		// No buttons are defined, so use default.
		var closeBtn = $("<button class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>");

		closeBtn.click(function() {
			if(typeof callback == 'function') {
				callback("btn-close", modalOuter);
			}
		});

		buttonsDiv.append(closeBtn);
	} else if(typeof buttons == 'object') {
		var buttonBuilder = function(buttonObj) {
			var btn = $("<button type=\"button\"></button>");

			btn.addClass(buttonObj.cssClasses);
			btn.html(buttonObj.caption);

			btn.click(function() {
				if(typeof callback == 'function') {
					if(buttonObj.clickResult) {
						callback(buttonObj.clickResult, modalOuter);
					} else {
						callback(buttonObj.caption, modalOuter);
					}
					
				}
			});
		
			return btn;
		};

		for(var i in buttons) {
			var btn = buttonBuilder(buttons[i]);
			buttonsDiv.append(btn);
		}

	} else if(typeof buttons == 'string') {
		buttonsDiv.append(buttons);
	}
	$("#MyModalFooter").html(buttonsDiv);

	modalOuter.modal();
}

function showModalWithButtons(heading, messageHtml, buttons, InfoObject, callback, CustomAction, EditEvent) {

	heading = heading || "Alert";
	var modalOuter = $("#myModal");
	$("#myModalLabel").html(heading);
	var alertMessage = $("<p></p>");	
	alertMessage.html(messageHtml);
	$("#MyModalBody").html(alertMessage);
	var buttonsDiv = $("<div></div>");
	$("#MyModalAlert").html("");
	if(typeof buttons == 'undefined') {
		// No buttons are defined, so use default.
		var closeBtn = $("<button class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>");

		closeBtn.click(function() {
			if(typeof callback == 'function') {
				callback("btn-close", modalOuter);
			}
		});

		buttonsDiv.append(closeBtn);
	} else if(typeof buttons == 'object') {
		var buttonBuilder = function(buttonObj) {
			var btn = $("<button type=\"button\"></button>");

			btn.addClass(buttonObj.cssClasses);
			btn.html(buttonObj.caption);

			btn.click(function() {
				if(typeof callback == 'function') {
					if(buttonObj.clickResult) {
						callback(buttonObj.clickResult, modalOuter);
					} else {
						callback(buttonObj.caption, modalOuter);
					}
					
				}
			});
		
			return btn;
		};

		for(var i in buttons) {
			var btn = buttonBuilder(buttons[i]);
			buttonsDiv.append(btn);
		}

	} else if(typeof buttons == 'string') {
		buttonsDiv.append(buttons);
	}
	$("#MyModalFooter").html(buttonsDiv);
	CustomAction();
	//Bind to variables
	for (var key in InfoObject) {
		if (typeof InfoObject[key] !== 'object') {
			SetInputItemValue("edit_" + key, InfoObject[key], function (value) {
				if (InfoObject == null) {
					return;
				}
				InfoObject[value.id.replace("edit_", "")] = GetInputItemValue(value.id);
				if (EditEvent != undefined && EditEvent != null) {
					EditEvent(value);
				}
			});
		}
	}
	modalOuter.modal();
}

function SanitizeString(string){
	return string.trim();
}

function FindItem(obj, objname, search) {
	try {
		if (typeof obj == "undefined") {
			return null;
		} else {
			for (var a = 0; a < obj.length; a++) {
				if (obj[a][objname] == search) {
					return obj[a];
				}
			}
			return null;
		}
	} catch (ex) {
		return null;
	}
}