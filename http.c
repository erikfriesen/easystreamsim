
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "include/GenericTypeDefs.h"
#include "global.h"
#include "http.h"
#include "b64.h"
#include "json.h"
#include "CRC.h"
#include "httpfiles.h"
#include "config.h"
#include "configsave.h"

/*typedef struct {
	pthread_t Thread;
	int session_fd;
	int Active;
} WebThreads;*/

static char * LastJson = NULL;
static int LastJsonLen = 0;
static pthread_mutex_t configmutex = PTHREAD_MUTEX_INITIALIZER;
static int SOCKET = -1;
static WebThreads ThreadPool[10];
static void GET_Handler(WebThreads * sessionptr, char * GetString);
static void POST_Handler(WebThreads * sessionptr, char * GetString, int count);
static int ParseJsonConfig(char * json, int len);
static void * handle_session(void * ptr);
static int WriteTcpSocket(int session_fd, char * buff, int length);
static void ManagePlayerParams(_PlayerSetup ps);
static void ManageDspSettings(_SymSettings s);
static void ManageHardwareParams(_HardwareSetup hs);
static void ManageStreamParams(_StreamSetup ss);

static char * strdup_max64(char * str) {
	int len = strlen(str);
	if (len > 64) {
		len = 64;
	}
	char * ret = malloc(len + 1);
	memcpy(ret, str, len + 1);
	return ret;
}

void ShutDownServer(pthread_t TopThread) {
	//shutdown threads
	//close sockets
	int a;
	pthread_cancel(TopThread);
	for (a = 0; a < 10; a++) {
		if (ThreadPool[a].Active) {
			pthread_cancel(ThreadPool[a].Thread);
		}
	}
	for (a = 0; a < 10; a++) {
		if (ThreadPool[a].Active) {
			close(ThreadPool[a].session_fd);
		}
	}
	close(SOCKET);
}

void * WebServerThread(void * ptr) {
	int a = 0;
	int iSetOption = 1;
	//char buff[2048];

	memset((void*) ThreadPool, 0, sizeof (ThreadPool));
	struct sockaddr_in si_me;
	struct sockaddr_in si_other;
	int slen = sizeof (si_other);
	if ((SOCKET = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
		MyLog("Cannot open TCP socket %s\n", strerror(errno));
		StartupError(10);
	} else {
		MyLog("TCP Socket %i opened\n", SOCKET);
	}

	if (setsockopt(SOCKET, SOL_SOCKET, SO_REUSEADDR, (char*) &iSetOption, sizeof (iSetOption)) == -1) {
		MyLog("Cannot set SO_REUSEADDR %s\n", strerror(errno));
		StartupError(10);
	}

	memset((void *) &si_me, 0, sizeof (si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(WebPort);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	while (a++ < 31) {
		if (bind(SOCKET, (const struct sockaddr *) &si_me, sizeof (si_me)) == -1) {
			if (a == 30) {
				MyLog("Cannot bind TCP socket to port %i\n", WebPort);
				StartupError(10);
			} else {
				Debug("TCP bind attempt %i failed %s\n", a + 1, strerror(errno));
			}
			sleep(2);
		} else {
			MyLog("TCP Socket %i bound to port %i\n", SOCKET, WebPort);
			break;
		}
	}


	if (listen(SOCKET, SOMAXCONN)) {
		MyLog("TCP Socket %i listen failed %i\n", SOCKET, WebPort);
		StartupError(11);
	}

	while (1) {
		usleep(5000); //Give threads a breather
		int session_fd = accept(SOCKET, 0, 0);
		if (session_fd == -1) {
			if (errno == EINTR) {

			}
			Debug("failed to accept connection (errno=%d)", errno);
		} else {
			//Search for available thread
			for (a = 0; a < 10; a++) {
				if (!ThreadPool[a].Active) {
					ThreadPool[a].Active = 1;
					ThreadPool[a].session_fd = session_fd;
					if (pthread_create(&ThreadPool[a].Thread, NULL, handle_session, &ThreadPool[a])) {
						Debug("failed to create child process (errno=%d)\n", errno);
						ThreadPool[a].Active = 0;
						a = 10;
					} else {
						if (pthread_detach(ThreadPool[a].Thread)) {
							Debug("Thread detach error (errno=%d)\n", errno);
						}
						break;
					}
				}
			}
			if (a == 10) {
				//Too many sessions, drop it
				close(session_fd);
			}
		}
	}
}

void * handle_session(void * ptr) {
	WebThreads * sessionptr = (WebThreads *) ptr;
	;
	char * buffer = malloc(8194);
	ssize_t ecount;
	time_t timer, comptimer;
	ssize_t count = read(sessionptr->session_fd, buffer, 8192);
	if (count < 0) {
		goto badpipe;
	}
	//Search for content-length
	buffer[count] = 0; //Lets make sure its null terminated
	if (StringsEqual("GET /", buffer)) {
		GET_Handler(sessionptr, buffer + 5);
	} else if (StringsEqual("POST /", buffer)) {
		char * LengthPointer = strstr(buffer, "Content-Length: ");
		char * StartPointer = strstr(buffer, "\r\n\r\n");
		if (LengthPointer != NULL && StartPointer != NULL) {
			StartPointer += 4;
			LengthPointer += 16;
			int ContentLength = strtol(LengthPointer, NULL, 10);
			int ActualLength = &buffer[count] - StartPointer;
			time(&timer);
			while (ActualLength < ContentLength) {
				if (8192 - count <= 0) {
					Debug("Out of space on POST rx\n");
					goto badpipe;
				}
				Debug("Waiting for remaining POST %i/%i\n", ActualLength, ContentLength);
				ecount = read(sessionptr->session_fd, &buffer[count], 8192 - count);
				if (ecount < 0) {
					goto badpipe;
				}
				count += ecount;
				ActualLength = &buffer[count] - StartPointer;
				time(&comptimer);
				if (comptimer - timer > 10) {
					Debug("Unfinished POST data\n");
					goto badpipe;
				}
			}
		}
		POST_Handler(sessionptr, buffer + 6, count - 6);
	} else {
		buffer[30] = 0;
		Debug("Web?:%s\n", buffer);
	}
badpipe:
	close(sessionptr->session_fd);
	free(buffer);
	sessionptr->Active = 0; //Race condition on flag?
	pthread_exit(NULL);
}

void GET_Handler(WebThreads * sessionptr, char * GetString) {
	int a, b;
	char ReplyBuff[5000];
	char tbuff[3000];
	size_t length = 0;
	if (StringsEqual(" HTTP/1.1", GetString) || StringsEqual("index.htm", GetString)) {
		//Index
		memcpy(GetString, "index.html ", 11);
	}
	if (StringsEqual("stream.mp3", GetString)) {
		length = GetHeader(ReplyBuff, "200 OK", "audio/mp3");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			printf("Streaming error #1, stopped\n");
			return;
		}
		unsigned short TailPtr = AudioHeadPtr - 1000;
		unsigned short Diff;
		while (1) {
			usleep(10000);
			Diff = AudioHeadPtr - TailPtr;
			if (Diff > 1500) {
				for (a = 0; a < 1500; a++) {
					ReplyBuff[a] = AudioBuffer[TailPtr++];
				}
				if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, 1500) < 0) {
					return;
				}
			}
		}
	} else if (StringsEqual("stream.raw", GetString)) {
		int TailPtr = OscBuffPtr;
		length = GetHeader(ReplyBuff, "200 OK", "application/octet-stream");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}

		for (a = 0; a < 2205; a++) {
			tbuff[a] = OscBuff[TailPtr++];
			if (TailPtr >= 2205) {
				TailPtr = 0;
			}
		}
		char * Tptr = ReplyBuff;
		base64_encodestate state;
		base64_init_encodestate(&state);
		Tptr += base64_encode_block(tbuff, 2205, Tptr, &state);
		Tptr += base64_encode_blockend(Tptr, &state);
		*Tptr = 0;

		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, Tptr - ReplyBuff) < 0) {
			return;
		}
	} else if (StringsEqual("stop.stop", GetString)) {
		length = GetHeader(ReplyBuff, "200 OK", "text/html");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
		WriteTcpSocket(sessionptr->session_fd, "Stopping....", 12);
		StartError = 1;
	} else if (StringsEqual("debug.lines", GetString)) {
		length = GetHeader(ReplyBuff, "200 OK", "text/html");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
#ifdef IS_DAEMON
		char * OutBuff = NULL;
		pthread_mutex_lock(&debugmutex);
		{
			length = 0;
			for (a = 0; a < DebugBufferLines; a++) {
				if (DebugBuffer[a] == NULL) {
					break;
				}
				length += strlen((char*) DebugBuffer[a]) + 4;
			}
			if (length > 0 && DebugBuffer[0] != NULL) {
				OutBuff = malloc(length + 10);
				char * str = OutBuff;
				strcpy(str, (char*) DebugBuffer[0]);
				strcat(str, "<br>");
				for (a = 1; a < DebugBufferLines; a++) {
					if (DebugBuffer[a] == NULL) {
						break;
					}
					strcat(str, (char*) DebugBuffer[a]);
					strcat(str, "<br>");
				}
			}
		}
		pthread_mutex_unlock(&debugmutex);
		if (OutBuff != NULL) {
			WriteTcpSocket(sessionptr->session_fd, OutBuff, length);
			free(OutBuff);
		}
#endif
	} else if (StringsEqual("io/config.json", GetString)) {
		length = GetHeader(ReplyBuff, "200 OK", "application/json");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
		char * ConfigXml = BuildXmlConfig();
		WriteTcpSocket(sessionptr->session_fd, ConfigXml, strlen(ConfigXml));
		free(ConfigXml);
	} else if (StringsEqual("io/info.json", GetString)) {
		length = GetHeader(ReplyBuff, "200 OK", "application/json");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
		char * ConfigStatus = BuildXmlStatus(0);
		WriteTcpSocket(sessionptr->session_fd, ConfigStatus, strlen(ConfigStatus));
		free(ConfigStatus);
	} else if (StringsEqual("debug.json", GetString)) {
		length = GetHeader(ReplyBuff, "200 OK", "application/json");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
		if (LastJson != NULL) {
			WriteTcpSocket(sessionptr->session_fd, LastJson, LastJsonLen);
		}
	} else {
		char * FileEnd = strchr(GetString, ' ');
		if (FileEnd != NULL) {
			*FileEnd = 0;
			char * tb = tbuff;
			tb += sprintf(tb, GetString);
			http_file * MyFile = FileList;
			while (MyFile->name != NULL) {
				if (strcmp(MyFile->name, tbuff) == 0) {
					break;
				}
				MyFile++;
			}
			if (MyFile->name == NULL) {
				//404
				printf("File %s not found\n", tbuff);
				length = GetHeader(ReplyBuff, "404 Not Found", "text/html");
				length += Get404(&ReplyBuff[length], GetString);
				if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
					return;
				}
			} else {
				//Feed the file out
				char * FileType = FileEnd - 3;
				char * OutType = "text/html";
				if (StringsEqual("ico", FileType)) {
					OutType = "image/x-icon";
				} else if (StringsEqual("css", FileType)) {
					OutType = "text/css";
				} else if (StringsEqual(".js", FileType)) {
					OutType = "application/javascript";
				}
				length = GetHeader(ReplyBuff, "200 OK", OutType);
				//Send headers
				if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
					return;
				}
				if (WriteTcpSocket(sessionptr->session_fd, MyFile->data, MyFile->Length) < 0) {
					return;
				}
			}
		} else {
			length = GetHeader(ReplyBuff, "500 Server Error", "text/html");
			if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
				return;
			}
		}
	}
}

void POST_Handler(WebThreads * sessionptr, char * GetString, int count) {
	int a;
	char ReplyBuff[5000];
	char tbuff[3000];
	size_t length = 0;
	//Find post start
	char * PostPointer = strstr(GetString, "\r\n\r\n");
	if (PostPointer == NULL) {
		length = GetHeader(ReplyBuff, "400 Bad request", "text/html");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
	}
	PostPointer += 4;
	int PostLength = count - (PostPointer - GetString);

	if (StringsEqual("io/config.json", GetString)) {
		if (ParseJsonConfig(PostPointer, PostLength)) {
			length = GetHeader(ReplyBuff, "400 Bad request", "text/html");
			if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
				return;
			}
			//Error copied into PostPointer
			WriteTcpSocket(sessionptr->session_fd, PostPointer, strlen(PostPointer));
		} else {
			length = GetHeader(ReplyBuff, "200 OK", "application/json");
			if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
				return;
			}
			char * ConfigXml = BuildXmlConfig();
			WriteTcpSocket(sessionptr->session_fd, ConfigXml, strlen(ConfigXml));
			free(ConfigXml);
		}
	} else {
		length = GetHeader(ReplyBuff, "400 Bad request", "text/html");
		if (WriteTcpSocket(sessionptr->session_fd, ReplyBuff, length) < 0) {
			return;
		}
	}
}

int WriteTcpSocket(int session_fd, char * buff, int length) {
	size_t index = 0;
	ssize_t count = 0;
	int sendlen = length;
	while (index < length) {
		count = send(session_fd, buff, sendlen, MSG_NOSIGNAL);
		if (count < 0) {
			if (errno == EINTR) continue;
			Debug("failed to write to socket (errno=%d)\n", errno);
			return -1;
		} else {
			index += count;
			buff += count;
			sendlen -= count;
		}
	}
	return length;
}

static int ParseJsonConfig(char * json, int len) {

	//Split into strings
	json_value* value;
	value = json_parse(json, len);
	if (value == NULL) {
		if (LastJson != NULL) {
			free(LastJson);
		}
		LastJson = malloc(len + 1);
		if (LastJson != NULL) {
			memcpy(LastJson, json, len);
			LastJson[len] = 0;
			LastJsonLen = len;
		}
		//strcpy(json, "Invalid json\n");
		Debug("Invalid json POST\r\n");
		goto badfinish;
	}
	int a;
	char hb[256];
	//_Control conset = {0};
	//_Info infoset = {0};
	_PlayerSetup playset = {0};
	_StreamSetup streamset = {0};
	_Services firmset = {0};
	_HardwareSetup hardset = {0};
	_SymSettings dspset = {0};
	//Info
	char * Unlock = json_find_string(value, "Config.Info.RenameUnlock");
	if (memcmp(Unlock, "true", 4) == 0) {
		free((char*) Info.DeviceName);
		Info.DeviceName = strdup(json_find_string(value, "Config.Info.DeviceName"));
	}
	Unlock = json_find_string(value, "Config.Info.DevIdUnlock");
	if (memcmp(Unlock, "true", 4) == 0) {
		char * DevId = json_find_string(value, "Config.Info.DeviceSerial");
		while (*DevId && a < 10) {
			Info.DeviceSerial[a++] = *DevId++;
			if (*DevId == ':') {
				DevId++;
			}
		}
	}
	Unlock = json_find_string(value, "Config.Info.AuthTokenUnlock");
	if (memcmp(Unlock, "true", 4) == 0) {
		free((char*) Info.AuthToken);
		Info.AuthToken = strdup(json_find_string(value, "Config.Info.AuthToken"));
	}
	Info.LastConfigStamp = json_find_int(value, "Config.Info.Unixtime");
	//Player
	a = json_find_int(value, "Config.Player.CtrlRxSelection");
	if (a != PlayerSetup.Ctrl_RxSelection) {
		PlayerSetup.Ctrl_RxSelection = a;
		PlayerSetup.RestartStreamIn = 1;
	}
	PlayerSetup.Ctrl_StreamIn = json_find_int(value, "Config.Player.CtrlStreamIn");
	//****  Further player setup
	for (a = 0; a < 32; a++) {
		sprintf(hb, "Config.Player.RxStreams[%i].Name", a);
		playset.RxStreams[a].Name = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Username", a);
		playset.RxStreams[a].Username = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Password", a);
		playset.RxStreams[a].Password = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Url", a);
		playset.RxStreams[a].Url = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Encoding", a);
		playset.RxStreams[a].Encoding = strcmp(json_find_string(value, hb), "AAC") == 0 ? EncType_AAC : EncType_MP3;
		playset.RxStreams[a].Active = strlen(playset.RxStreams[a].Url) > 0;
	}
	//Stream
	StreamSetup.ControlType = json_find_int(value, "Config.Stream.ControlType");
	StreamSetup.Mounts[0].Ctrl_Stream = json_find_int(value, "Config.Stream.Mounts[0].CtrlStream");
	StreamSetup.Mounts[1].Ctrl_Stream = json_find_int(value, "Config.Stream.Mounts[1].CtrlStream");
	streamset.BitRate = json_find_int(value, "Config.Stream.BitRate");
	streamset.EncType = json_find_int(value, "Config.Stream.EncType");
	StreamSetup.StreamOnLevel = json_find_int(value, "Config.Stream.StreamOnLevel");
	StreamSetup.StreamOnDelay = json_find_int(value, "Config.Stream.StreamOnDelay");
	streamset.Mounts[0].SourceName = json_find_string(value, "Config.Stream.Mounts[0].SourceName");
	streamset.Mounts[0].UserName = json_find_string(value, "Config.Stream.Mounts[0].UserName");
	streamset.Mounts[0].Password = json_find_string(value, "Config.Stream.Mounts[0].Password");
	streamset.Mounts[0].Name = json_find_string(value, "Config.Stream.Mounts[0].Name");
	streamset.Mounts[0].URL = json_find_string(value, "Config.Stream.Mounts[0].URL");
	streamset.Mounts[0].Port = json_find_int(value, "Config.Stream.Mounts[0].Port");
	streamset.Mounts[0].StreamActive = json_find_int(value, "Config.Stream.Mounts[0].StreamActive");
	streamset.Mounts[1].SourceName = json_find_string(value, "Config.Stream.Mounts[1].SourceName");
	streamset.Mounts[1].UserName = json_find_string(value, "Config.Stream.Mounts[1].UserName");
	streamset.Mounts[1].Password = json_find_string(value, "Config.Stream.Mounts[1].Password");
	streamset.Mounts[1].Name = json_find_string(value, "Config.Stream.Mounts[1].Name");
	streamset.Mounts[1].URL = json_find_string(value, "Config.Stream.Mounts[1].URL");
	streamset.Mounts[1].Port = json_find_int(value, "Config.Stream.Mounts[1].Port");
	streamset.Mounts[1].StreamActive = json_find_int(value, "Config.Stream.Mounts[1].StreamActive");
	//Firmware
	firmset.UpdateUrl = json_find_string(value, "Config.Services.UpdateUrl");
	if (strcmp(firmset.UpdateUrl, SysServices.UpdateUrl) != 0) {
		free(SysServices.UpdateUrl);
		SysServices.UpdateUrl = strdup_max64(firmset.UpdateUrl);
	}
	SysServices.DoUpdate = json_find_int(value, "Config.Services.DoUpdate");

	firmset.ControlUrl = json_find_string(value, "Config.Services.ControlUrl");
	if (strcmp(firmset.ControlUrl, SysServices.ControlUrl) != 0) {
		free(SysServices.ControlUrl);
		SysServices.ControlUrl = strdup_max64(firmset.ControlUrl);
	}

	firmset.PreAuthToken = json_find_string(value, "Config.Services.PreAuthToken");
	if (strcmp(firmset.PreAuthToken, SysServices.PreAuthToken) != 0) {
		free(SysServices.PreAuthToken);
		SysServices.PreAuthToken = strdup(firmset.PreAuthToken);
	}
	SysServices.PollPeriod = json_find_int(value, "Config.Services.PollPeriod");
	//Hardware
	hardset.AutoLimiter = json_find_int(value, "Config.Hardware.AutoLimiter");
	hardset.HardwareGain = json_find_int(value, "Config.Hardware.HardwareGain");
	hardset.SampleRate = json_find_int(value, "Config.Hardware.SampleRate");
	hardset.InVolume = json_find_double(value, "Config.Hardware.InVolume");
	hardset.OutVolume = json_find_int(value, "Config.Hardware.OutVolume");
	//DSP
	char * SymIp = json_find_string(value, "Config.DSP.IP");
	char * pch = strtok(SymIp, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[0] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[1] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[2] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[3] = strtol(pch, NULL, 10);

	dspset.Port = json_find_int(value, "Config.DSP.Port");
	dspset.TCP = json_find_int(value, "Config.DSP.TCP");
	dspset.RxSelection = json_find_int(value, "Config.DSP.RxSelection");
	DspSettings.Stream1 = json_find_int(value, "Config.DSP.Stream1");
	DspSettings.Stream2 = json_find_int(value, "Config.DSP.Stream2");
	DspSettings.StreamIn = json_find_int(value, "Config.DSP.StreamIn");
	DspSettings.IsStreaming1 = json_find_int(value, "Config.DSP.IsStreaming1");
	DspSettings.IsStreaming2 = json_find_int(value, "Config.DSP.IsStreaming2");
	DspSettings.AudioPeak = json_find_int(value, "Config.DSP.AudioPeak");
	DspSettings.AudioLevel = json_find_int(value, "Config.DSP.AudioLevel");
	//Validate

	ManagePlayerParams(playset);
	ManageStreamParams(streamset);
	ManageHardwareParams(hardset);
	ManageDspSettings(dspset);
	json_value_free(value);
	//Save config files
	SaveCurrentConfig();
	Debug("ParseJsonConfig success\r\n");
	return 0;
badfinish:
	Debug("ParseJsonConfig error\r\r\n");
	return 1;
}

static void ManagePlayerParams(_PlayerSetup ps) {
	int a, i;
	int RestartStreamIn = 0;
	i = PlayerSetup.Ctrl_RxSelection - 1;
	if (i < 0 || i > 31) {
		i = 0;
	}
	for (a = 0; a < 32; a++) {
		if (strcmp(ps.RxStreams[a].Name, PlayerSetup.RxStreams[a].Name) != 0) {
			free(PlayerSetup.RxStreams[a].Name);
			PlayerSetup.RxStreams[a].Name = strdup_max64(ps.RxStreams[a].Name);
			RestartStreamIn = 1;
		}
		if (strcmp(ps.RxStreams[a].Username, PlayerSetup.RxStreams[a].Username) != 0) {
			free(PlayerSetup.RxStreams[a].Username);
			PlayerSetup.RxStreams[a].Username = strdup_max64(ps.RxStreams[a].Username);
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (strcmp(ps.RxStreams[a].Password, PlayerSetup.RxStreams[a].Password) != 0) {
			free(PlayerSetup.RxStreams[a].Password);
			PlayerSetup.RxStreams[a].Password = strdup_max64(ps.RxStreams[a].Password);
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (strcmp(ps.RxStreams[a].Url, PlayerSetup.RxStreams[a].Url) != 0) {
			free(PlayerSetup.RxStreams[a].Url);
			PlayerSetup.RxStreams[a].Url = strdup_max64(ps.RxStreams[a].Url);
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (ps.RxStreams[a].Encoding != PlayerSetup.RxStreams[a].Encoding) {
			PlayerSetup.RxStreams[a].Encoding = ps.RxStreams[a].Encoding;
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (ps.RxStreams[a].Active != PlayerSetup.RxStreams[a].Active) {
			PlayerSetup.RxStreams[a].Active = ps.RxStreams[a].Active;
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
	}
	if (RestartStreamIn) {
		PlayerSetup.RestartStreamIn = 1;
	}
}

static void ManageStreamParams(_StreamSetup ss) {

	int Restart1 = 0;
	int Restart2 = 0;
	int Restart = 0;
	if (ss.BitRate != StreamSetup.BitRate) {
		StreamSetup.BitRate = ss.BitRate;
		Restart = 1;
	}
	if (ss.EncType != StreamSetup.EncType) {
		StreamSetup.EncType = ss.EncType;
		Restart = 1;
	}
	if (Restart) {
		Control.RestartEncoder = 1;
		StreamSetup.Mounts[0].RestartStream = 1;
		StreamSetup.Mounts[1].RestartStream = 1;
		Debug("Restarting Encoder\r\n");
	}

	if (strcmp(ss.Mounts[0].SourceName, StreamSetup.Mounts[0].SourceName) != 0) {
		free(StreamSetup.Mounts[0].SourceName);
		StreamSetup.Mounts[0].SourceName = strdup_max64(ss.Mounts[0].SourceName);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].UserName, StreamSetup.Mounts[0].UserName) != 0) {
		free(StreamSetup.Mounts[0].UserName);
		StreamSetup.Mounts[0].UserName = strdup_max64(ss.Mounts[0].UserName);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].Password, StreamSetup.Mounts[0].Password) != 0) {
		free(StreamSetup.Mounts[0].Password);
		StreamSetup.Mounts[0].Password = strdup_max64(ss.Mounts[0].Password);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].Name, StreamSetup.Mounts[0].Name) != 0) {
		free(StreamSetup.Mounts[0].Name);
		StreamSetup.Mounts[0].Name = strdup_max64(ss.Mounts[0].Name);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].URL, StreamSetup.Mounts[0].URL) != 0) {
		free(StreamSetup.Mounts[0].URL);
		StreamSetup.Mounts[0].URL = strdup_max64(ss.Mounts[0].URL);
		Restart1 = 1;
	}

	if (ss.Mounts[0].Port != StreamSetup.Mounts[0].Port) {
		StreamSetup.Mounts[0].Port = ss.Mounts[0].Port;
		Restart1 = 1;
	}

	if (ss.Mounts[0].StreamActive != StreamSetup.Mounts[0].StreamActive) {
		StreamSetup.Mounts[0].StreamActive = ss.Mounts[0].StreamActive;
		Restart1 = 1;
	}

	if (Restart1) {
		StreamSetup.Mounts[0].RestartStream = 1;
		StreamSetup.Mounts[0].AuthError = 0;
		Debug("Restarting Stream 1\r\n");
	}

	if (strcmp(ss.Mounts[1].SourceName, StreamSetup.Mounts[1].SourceName) != 0) {
		free(StreamSetup.Mounts[1].SourceName);
		StreamSetup.Mounts[1].SourceName = strdup_max64(ss.Mounts[1].SourceName);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].UserName, StreamSetup.Mounts[1].UserName) != 0) {
		free(StreamSetup.Mounts[1].UserName);
		StreamSetup.Mounts[1].UserName = strdup_max64(ss.Mounts[1].UserName);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].Password, StreamSetup.Mounts[1].Password) != 0) {
		free(StreamSetup.Mounts[1].Password);
		StreamSetup.Mounts[1].Password = strdup_max64(ss.Mounts[1].Password);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].Name, StreamSetup.Mounts[1].Name) != 0) {
		free(StreamSetup.Mounts[1].Name);
		StreamSetup.Mounts[1].Name = strdup_max64(ss.Mounts[1].Name);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].URL, StreamSetup.Mounts[1].URL) != 0) {
		free(StreamSetup.Mounts[1].URL);
		StreamSetup.Mounts[1].URL = strdup_max64(ss.Mounts[1].URL);
		Restart2 = 1;
	}

	if (ss.Mounts[1].Port != StreamSetup.Mounts[1].Port) {
		StreamSetup.Mounts[1].Port = ss.Mounts[1].Port;
		Restart2 = 1;
	}
	if (ss.Mounts[1].StreamActive != StreamSetup.Mounts[1].StreamActive) {
		StreamSetup.Mounts[1].StreamActive = ss.Mounts[1].StreamActive;
		Restart1 = 1;
	}
	if (Restart2) {
		StreamSetup.Mounts[1].RestartStream = 1;
		StreamSetup.Mounts[1].AuthError = 0;
		Debug("Restarting Stream 2\r\n");
	}
}

static void ManageHardwareParams(_HardwareSetup hs) {
	int Restart = 0;
	if (hs.HardwareGain != HardwareSetup.HardwareGain) {
		HardwareSetup.HardwareGain = hs.HardwareGain;
		Restart = 1;
	}
	if (hs.SampleRate != HardwareSetup.SampleRate) {
		HardwareSetup.SampleRate = hs.SampleRate;
		Restart = 1;
	}
	if (hs.InVolume != HardwareSetup.InVolume) {
		HardwareSetup.InVolume = hs.InVolume;
		Restart = 1;
	}
	if (hs.OutVolume != HardwareSetup.OutVolume) {
		HardwareSetup.OutVolume = hs.OutVolume;
		if (HardwareSetup.OutVolume<-48) {
			HardwareSetup.OutVolume = -48;
		} else if (HardwareSetup.OutVolume > 0) {
			HardwareSetup.OutVolume = 0;
		}
		Restart = 1;
	}
	if (hs.AutoLimiter != HardwareSetup.AutoLimiter) {
		HardwareSetup.AutoLimiter = hs.AutoLimiter;
	}
	if (Restart) {
		HardwareSetup.InInitErr = 0;
		HardwareSetup.InInitdone = 0;
		HardwareSetup.InitNeeded = 1;
		Debug("Restarting Audio\r\n");
	}
}

static void ManageDspSettings(_SymSettings s) {

	int RestartStreamIn = 0;
	int RestartNeeded = 0;
	if (s.IP.Val != DspSettings.IP.Val) {
		DspSettings.IP.Val = s.IP.Val;
		RestartNeeded = 1;
	}
	if (s.Port != DspSettings.Port) {
		DspSettings.Port = s.Port;
		RestartNeeded = 1;
	}
	if (s.TCP != DspSettings.TCP) {
		DspSettings.TCP = s.TCP;
		RestartNeeded = 1;
	}

	if (s.RxSelection != DspSettings.RxSelection) {
		DspSettings.RxSelection = s.RxSelection;
		RestartStreamIn = 1;
	}

	if (RestartStreamIn) {
		PlayerSetup.RestartStreamIn = 1;
	}
	if (RestartNeeded) {
		DspSettings.RestartNeeded = 1;
		Debug("Restarting Symetrix\r\n");
	}
}

