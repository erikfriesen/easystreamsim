/* 
 * File:   main.c
 * Author: Erik
 *
 * Created on September 2, 2015, 5:33 PM
 */

#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdlib.h>
#include <string.h>
#include <config.h>
#include <arpa/inet.h>
#include <errno.h>
#include <pthread.h>
#include <syslog.h>
#include <signal.h>
#include <sys/mman.h>
#include "b64.h"
#include "include/GenericTypeDefs.h"
#include "global.h"
#include "htmlhelpers.h"
#include "http.h"
#include "json.h"
#include "CRC.h"
#include "player.h"
#include "iceconfig.h"
#include "configsave.h"
#include <math.h>
#include "RemControl.h"
#ifdef TS4900_BASEBOARD
#include "eeprom.h"
#endif

#define PORT 20000
#define AudioBuffSize 65535
volatile int StartError = 0;

volatile char * DeviceName;
pthread_mutex_t namechangemutex = PTHREAD_MUTEX_INITIALIZER;

volatile int PeakDetector;
volatile int AudioLevel;
volatile int BuffInMs = 0;
volatile int NewUrls = 0;

volatile _Info Info = {
    .DeviceName = "",
    .AuthToken = "",
    .DeviceSerial =
    {'1', '2', '3', 'A', 'B', 'C', '7', '8', '9'}
};

volatile _PlayerSetup PlayerSetup = {
	.Ctrl_RxSelection = 1,
	.Ctrl_StreamIn = 0,
	.StreamListenStatus = 0,
	.StreamListenStatus = 0,
	.RxStreams[0] =
	{
		.Active = 1,
		.Name = "",
		.Url = "",
		.Password = "",
		.Username = ""
	}
};

volatile _StreamSetup StreamSetup = {
	.BitRate = 64,
	.EncType = EncType_MP3,
	.StreamOnLevel = 2,
	.StreamOnDelay = 300,
	.ControlType = SCT_Manual,
	.Mounts[0] =
	{
		.StreamActive = 1,
		.Ctrl_Stream = 0,
		.StreamConnected = 0,
		.AuthError = 0,
		.SourceName = "",
		.UserName = "",
		.Password = "",
		.Name = "",
		.URL = "",
		.Port = 8000,
		//Private
		.RestartStream = 1,
		.StreamConnectErr = 0
	},
	.Mounts[1] =
	{
		.StreamActive = 0,
		.Ctrl_Stream = 0,
		.StreamConnected = 0,
		.AuthError = 0,
		.SourceName = "",
		.UserName = "",
		.Password = "",
		.Name = "",
		.URL = "",
		.Port = 8000,
		//Private
		.RestartStream = 1,
		.StreamConnectErr = 0
	},

};

volatile _Services SysServices = {
    .UpdateUrl = "",
    .DoUpdate = 0,
    .UpdateStatus = "done",
	 .ControlStatus = "Disconnected"
};

volatile _HardwareSetup HardwareSetup = {
    .HardwareGain = 0,
    .SampleRate = 0,
    .InVolume = 0,
    .OutVolume = 0,
    .AutoLimiter = 1,
    //Private
    .NewInVolume = 0,
    .NewOutVolume =0,
    .InInitErr = 0,
    .InInitdone = 0,
    .InitNeeded = 1
};

volatile _SymSettings DspSettings = {
    .Type = 0,
    .IP = {.v =	{192, 168, 0, 210}},
    .Port = 48630,
    .Stream1 = 5000,
    .Stream2 = 5001,
    .StreamIn = 5002,
    .RxSelection = 5003,
    .AudioPeak = 5004,
    .AudioLevel = 5005,
    .IsStreaming1 = 5006,
    .IsStreaming2 = 5007,	
    .RestartNeeded = 1,
    .Type = 0
};

volatile _Control Control = {
    .RestartEncoder = 1
};


//Audio buffers
volatile char OscBuff[2205];
volatile int OscBuffPtr = 0;
volatile unsigned char * AudioBuffer;
volatile unsigned short AudioHeadPtr = 0;
volatile unsigned short AudioTailPtr1 = 0;
volatile unsigned short AudioTailPtr2 = 0;
volatile sig_atomic_t ProgramRunning = 1;

#ifdef IS_DAEMON
volatile char * DebugBuffer[DebugBufferLines] = {0};
pthread_mutex_t debugmutex = PTHREAD_MUTEX_INITIALIZER;
#endif
void * AudioThread(void * ptr);
void * StreamingThread1(void * ptr);
void * StreamingThread2(void * ptr);
void * AnnounceThread(void * ptr);
void * CommunicationThread(void * ptr);
void * WebServerThread(void * ptr);
void StartupError(int value);
void MyLog(const char* format, ...);
char * BuildLogin(int stream);
//char * BuildXmlConfig(void);
//char * BuildXmlInfo(void);
uint16_t rms_filter(uint16_t sample);
void FillSine(short* Buff, int Freq, int Length, float * SineLocator);
void terminate(int signum);

int main(int argc, char** argv) {
	MyLog("EasyStreamSim start\n");
#ifdef IS_DAEMON
	pid_t pid, sid;
	pid = fork();
	if (pid < 0) {
		MyLog("Fork Failure\n");
		exit(EXIT_FAILURE);
	}
	if (pid > 0) {
		exit(EXIT_SUCCESS);
	}

	umask(0);
	sid = setsid();
	if (sid < 0) {
		/* Log any failures here */
		MyLog("sid < 0, closing\n");
		exit(EXIT_FAILURE);
	}

	MyLog("Starting EasyStreamSim Version " VERSION " PID %i SID %i\n", pid, sid);
	/* Change the current working directory */
	if ((chdir("/")) < 0) {
		/* Log any failures here */
		MyLog("chdir failed\n");
		exit(EXIT_FAILURE);
	}
#endif
	int a, err, c;
	InitCRC(0xFFFF);

	struct sigaction action;
	memset(&action, 0, sizeof (struct sigaction));
	action.sa_handler = terminate;
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGINT, &action, NULL);
	//Do mallocs here before threads take off
	AudioBuffer = malloc(AudioBuffSize);
	if(AudioBuffer == NULL) {
		MyLog("AudioBuffer malloc failure\n");
		return (EXIT_FAILURE);
	}
	PcmBuffer = malloc(PcmBufferSize * 2);
	if (PcmBuffer == NULL) {
		MyLog("PcmBuffer malloc failure\n");
		return (EXIT_FAILURE);
	}
#ifdef TS4900_BASEBOARD
	EEPROMInit();
#endif
	if (ConfigManager()) {
		MyLog("Configuration error\n");
		return (EXIT_FAILURE);
	}
	pthread_t AuThread, St1Thread, PlAuThread, WebThread, AncThread, CtrlThread;
	signal(SIGPIPE, SIG_IGN);

	if (pthread_create(&AuThread, NULL, AudioThread, NULL)) {
		MyLog("Could not create Audio thread\n");
		return (EXIT_FAILURE);
	}

	if (pthread_create(&PlAuThread, NULL, PlaybackAudioThread, NULL)) {
		MyLog("Could not create Audio thread\n");
		return (EXIT_FAILURE);
	}

	if (pthread_create(&St1Thread, NULL, StreamingThread1, NULL)) {
		MyLog("Could not create Streaming thread 1\n");
		return (EXIT_FAILURE);
	}

	if (pthread_create(&WebThread, NULL, WebServerThread, NULL)) {
		MyLog("Could not create Webserver thread\n");
		return (EXIT_FAILURE);
	}

	if (pthread_create(&AncThread, NULL, AnnounceThread, NULL)) {
		MyLog("Could not create Announce thread\n");
		return (EXIT_FAILURE);
	}


	if (pthread_create(&CtrlThread, NULL, RemControlThread, NULL)) {
		MyLog("Could not create Remote control thread\n");
		return (EXIT_FAILURE);
	}
	
	while (ProgramRunning) {
		usleep(100000);
		if (StartError) {
			break;
		}
	}
	printf("Shutting down EasyStream webserver\n");
	ShutDownServer(WebThread);
	printf("IceStreamer exiting\n");
	usleep(100000);
	return (EXIT_SUCCESS);
}

void terminate(int signum)
{
	ProgramRunning = 0;
}

void * AudioThread(void * ptr) {
#define AudioOffTimeout 5

	int a, b, err, c, d;
	unsigned int peakhelper = 0;
	unsigned int audiolevelhelper = 0;
	unsigned long long RmsSum;
	int MinStartCount = StdSampleRate;
	int RmsSumCnt;
	int OscPeakCnt = 0;
	int AudioHardwareError = 0;
	time_t timer, LastData, comparetime;
	time(&LastData);
	unsigned int rrate = StdSampleRate;
	unsigned char * outputBuffer = malloc(32767);
	unsigned char * inputBuffer = malloc(32767);
	if(inputBuffer == NULL || outputBuffer == NULL) {
		MyLog("AudioThread Malloc failure\n");
		StartupError(1);
	}
	short * InBuffPtr = (short*) inputBuffer;

	while (1) {
		usleep(10000); //Give threads a breather
		if (Control.RestartEncoder) {
			Control.RestartEncoder = 0;
		}
	}
}

void * StreamingThread1(void * ptr) {
	int a, b, SendCount;
	ssize_t BytesSent;
	int Burst = 1;
	time_t timer, timercomp, OnDelay;
	time(&timer);
	time(&OnDelay);
	unsigned char * RxBuff = malloc(5000);
	unsigned char * OutBuff = malloc(65535);
	if(RxBuff == NULL || OutBuff == NULL){
		MyLog("StreamThread1 Unable to allocate RxBuff or OutBuff\n");
		StartupError(50);
	}
	StreamSetup.Mounts[0].StreamConnected = 0;
	StreamSetup.Mounts[0].StreamConnectErr = StrmErr_None;
	signal(SIGPIPE, SIG_IGN);
	int sockfd = -1;
	struct hostent *server;
	struct sockaddr_in serv_addr;
	struct timeval tv;
	int BuffSize = 65535;
	socklen_t optlen = 4;
	tv.tv_sec = 10;
	tv.tv_usec = 0;
	while (1) {
		usleep(10000); //Give threads a breather
		switch (StreamSetup.ControlType) {
			case SCT_Manual:
			case SCT_Manual_IR:
				break;
			case SCT_SymetrixReport:
			case SCT_AudioLevel:
				if (ConvertToPercent(PeakDetector) > StreamSetup.StreamOnLevel) {
					time(&OnDelay);
					if(!StreamSetup.Mounts[0].Ctrl_Stream){
						StreamSetup.Mounts[0].Ctrl_Stream = 1;
						MyLog("Stream started at audio level = %i\n", ConvertToPercent(PeakDetector));
						//Debug("Stream started at audio level = %i\n", PeakDetector);
					}
				} else {
					if (StreamSetup.Mounts[0].Ctrl_Stream) {
						time(&timercomp);
						long ET = timercomp - OnDelay;
						if (ET > StreamSetup.StreamOnDelay) {
							StreamSetup.Mounts[0].Ctrl_Stream = 0;
							MyLog("Stream stopped at level = %i\n", ConvertToPercent(PeakDetector));
							//Debug("Stream stopped at level = %i\n", PeakDetector);
						}
					}
				}
				break;
			case SCT_Symetrix:
				//Control.Ctrl_Stream = SymSettings.Ctrl_Stream;
				break;
		}
		if (StreamSetup.Mounts[0].Ctrl_Stream) {
			StreamSetup.Mounts[0].StreamConnected = 1;
		} else {
			StreamSetup.Mounts[0].StreamConnected = 0;
		}
		if (StreamSetup.Mounts[1].Ctrl_Stream) {
			StreamSetup.Mounts[1].StreamConnected = 1;
		} else {
			StreamSetup.Mounts[1].StreamConnected = 0;
		}
	}
}

void * AnnounceThread(void * ptr) {
#define ListenPort 40404
#define BUFLEN 256
	char buf[BUFLEN];
	char Reply[256];
	char MacAddress[32];
	int AncSocket = 0;
	struct sockaddr_in si_me;
	struct sockaddr_in si_other;
	if ((AncSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
		MyLog("Unable to open upd socket\n");
		StartupError(100);
	} else {
		MyLog("UDP announce socket opened\n");
	}
	memset((void *) &si_me, 0, sizeof (si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_port = htons(ListenPort);
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	if (bind(AncSocket, (const struct sockaddr *) &si_me, sizeof (si_me)) == -1) {
		MyLog("Unable to open bind upd socket to port %i\n", ListenPort);
	} else {
		MyLog("Announce bound to socket %i\n", AncSocket);
	}

	FILE * ReadMac = fopen("/sys/class/net/eth0/address", "r");
	if (ReadMac == NULL) {
		MyLog("Unable to read mac %s\n", strerror(errno));
		sprintf(MacAddress, "00:00:00:00:00:00");
	} else {
		int ReadCount = fread(MacAddress, 1, 17, ReadMac);
		if (ReadCount != 17) {
			sprintf(MacAddress, "00:00:00:00:00:00");
		} else {
			MacAddress[17] = 0; //Null terminate
		}
	}
	Debug("Announce mac %s\n", MacAddress);
	while (1) {
		int slen = sizeof (si_other);
		int ByteCount = recvfrom(AncSocket, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen);
		if (ByteCount == -1) {
			Debug("Announce byte count -1");
		}
		buf[ByteCount] = 0;
		Debug("Received packet from %s:%d\nData: %s\n\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), buf);
		char * tb = Reply;
		
		pthread_mutex_lock(&namechangemutex);
		{
			tb += sprintf(tb, "%s\r\n%s\r\nPeak = %i", DeviceName, MacAddress, ConvertToPercent(PeakDetector));
		}
		pthread_mutex_unlock(&namechangemutex);
		
		if (sendto(AncSocket, Reply, strlen(Reply), 0, (struct sockaddr *) &si_other, slen) == -1) {
			Debug("Announce packet failure at %i:%i\n", AncSocket, PORT);
		} else {
			Debug("Announce = %s\n", Reply);
		}
	}
}
/*
char * BuildLogin(int stream) {
	int a;
	char* input = (char*) malloc(2048);
	char* Base64Password = (char*) malloc(2048);
	char* Base64Passwordptr = Base64Password;
	if (stream == 0) {
		sprintf(input, "%s:%s", StreamParams.UserName1, StreamParams.Password1);
	} else {
		sprintf(input, "%s:%s", StreamParams.UserName2, StreamParams.Password2);
	}
	base64_encodestate state;
	base64_init_encodestate(&state);
	Base64Passwordptr += base64_encode_block(input, strlen(input), Base64Password, &state);
	Base64Passwordptr += base64_encode_blockend(Base64Passwordptr, &state);
	*Base64Passwordptr = 0;
	char * buff = (char*) malloc(4096);
	char * buffptr = buff;
	if (stream == 0) {
		buffptr += sprintf(buffptr, "SOURCE /%s HTTP/1.0\n", StreamParams.SourceName1);
	} else {
		buffptr += sprintf(buffptr, "SOURCE /%s HTTP/1.0\n", StreamParams.SourceName2);
	}
	buffptr += sprintf(buffptr, "Authorization: Basic %s\n", Base64Password);
	buffptr += sprintf(buffptr, "icy-br: %i\n", LameParams.BitRate);
	buffptr += sprintf(buffptr, "icy-notice1: EasyStream/1.0\n");
	buffptr += sprintf(buffptr, "Content-Type: audio/mp3\n");
	if (stream == 0) {
		buffptr += sprintf(buffptr, "icy-name: %s\n", StreamParams.Name1);
		buffptr += sprintf(buffptr, "icy-public: %i\n", StreamParams.Public1);
	} else {
		buffptr += sprintf(buffptr, "icy-name: %s\n", StreamParams.Name2);
		buffptr += sprintf(buffptr, "icy-public: %i\n", StreamParams.Public2);
	}
	buffptr += sprintf(buffptr, "User-Agent: EasyStream\n\n");
	free(input);
	free(Base64Password);
	return buff;
}*/

void StartupError(int value) {
	StartError = value;
	while(1);//Wait for upper to stop thread
}

void Debug(const char* format, ...) {
	va_list argptr;
	va_start(argptr, format);
#ifdef IS_DAEMON	
	int a;
	char * buff = NULL;
	char * tbuff = NULL;
	int Len = vasprintf(&buff, format, argptr);
	if (Len == -1 || buff == NULL) {
		return;
	}
	struct timeval tv;
	gettimeofday(&tv, NULL);
	int Tlen = asprintf(&tbuff, "%05u.%03u : ", (tv.tv_sec & 0xFFFF), (tv.tv_usec / 1000));
	if (Tlen == -1 || tbuff == NULL) {
		return;
	}
	//Search for empty lines
	for (a = 0; a < DebugBufferLines; a++) {
		if(DebugBuffer[a] == NULL){
			break;
		}
	}
	pthread_mutex_lock(&debugmutex);
	{
		if (a == DebugBufferLines) {
			//Shift down
			free((void*) DebugBuffer[0]);
			for (a = 0; a < DebugBufferLines - 1; a++) {
				DebugBuffer[a] = DebugBuffer[a + 1];
			}
			a = DebugBufferLines - 1;
		}
		DebugBuffer[a] = malloc(Tlen + Len + 1);
		if (DebugBuffer[a] != NULL) {
			memcpy((void*) DebugBuffer[a], tbuff, Tlen);
			memcpy((void*) DebugBuffer[a] + Tlen, buff, Len);
			DebugBuffer[a][Tlen + Len] = 0;
		}
	}
	pthread_mutex_unlock(&debugmutex);
	free(buff);
	free(tbuff);
#else
	vfprintf(stderr, format, argptr);
	va_end(argptr);
#endif
}

void MyLog(const char* format, ...) {
	va_list argptr;
	va_start(argptr, format);
#ifdef IS_DAEMON	
	openlog("Easy-Stream", LOG_CONS | LOG_PID, LOG_USER);
	vsyslog(LOG_INFO, format, argptr);
	closelog();
	void *arg = __builtin_apply_args();
	void *ret = __builtin_apply((void*)Debug, arg, 500);
    __builtin_return(ret);
	//Debug(format, arg); //Forward to debug output
	va_end(argptr);
#else
	vfprintf(stderr, format, argptr);
	va_end(argptr);
#endif
}

#define INITIAL 0  /* Initial value of the filter memory. */
#define SAMPLES 128

uint16_t rms_filter(uint16_t sample) {
	static uint16_t rms = INITIAL;
	static uint32_t sum_squares = 1UL * SAMPLES * INITIAL * INITIAL;

	sum_squares -= sum_squares / SAMPLES;
	sum_squares += (uint32_t) sample * sample;
	if (rms == 0) rms = 1; /* do not divide by zero */
	rms = (rms + sum_squares / SAMPLES / rms) / 2;
	return rms;
}

int ConvertToDb10(int Value) {
	if(!Value){
		Value = 1;
	}
	return 180 - (100 * log10((double)32767 / Value));
}


double time_diff(struct timeval x , struct timeval y)
{
    double x_ms , y_ms , diff;
     
    x_ms = (double)x.tv_sec*1000000 + (double)x.tv_usec;
    y_ms = (double)y.tv_sec*1000000 + (double)y.tv_usec;
     
    diff = (double)y_ms - (double)x_ms;
     
    return abs(diff);
}

int recv_catch_eintr(int socket, void * buf, size_t len, int flags, int timeout_ms) {
	int a;
	struct timeval tvs, tvd;
	gettimeofday(&tvs, NULL);
	while (1) {
		a = recv(socket, buf, len, flags);
		if (a > 0) {
			break;
		} else if (a <= 0 && errno != EINTR) {
			break;
		}
		gettimeofday(&tvd, NULL);
		if (time_diff(tvd, tvs) > timeout_ms * 1000) {
			break;
		}
	}
}

int LimitInt(int val, int min, int max) {
	if (val < min) {
		return min;
	} else if (val > max) {
		return max;
	} else {
		return val;
	}
}

