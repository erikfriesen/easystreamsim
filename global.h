/* 
 * File:   global.h
 * Author: Erik
 *
 * Created on September 12, 2015, 9:11 AM
 */

#ifndef GLOBAL_H
#define	GLOBAL_H
#include "iceconfig.h"
#define PUBLICINTERFACE
/*
#ifdef TS4900_BASEBOARD
#define USE_EEPROM
#ifndef USE_AUTO_VOLUME
#define USE_AUTO_VOLUME
#endif
#define HW_AudioInitScript "Headphone Mux,0:Mic Volume,0:Capture Mux,1:Dap Mux,0:Capture Attenuate Switch (-6dB),0"
#define HW_AudioDevice "default"
#define HW_AudioInitDevice "hw:0"
#define HW_AudioInput		"Capture Volume"
#define HW_AudioOutput		"PCM Playback Volume"
#else
#ifdef USE_AUTO_VOLUME
#define HW_AudioInitScript "Headphone Mux,0:Mic Volume,0:Capture Attenuate Switch (-6dB),0:Capture Mux,0:Dap Mux,0"
#else
#define HW_AudioInitScript "Headphone Mux,0:Mic Volume,0:Capture Attenuate Switch (-6dB),0:Capture Mux,0"
#endif
#define HW_AudioDevice "default"
#define HW_AudioInitDevice "hw:0"
#define HW_AudioInput "Capture Volume"
#define HW_AudioOutput "PCM Playback Volume"
#endif

#ifdef USE_AUTO_VOLUME
#define HW_AudioAvcEnable	"AVC enable"
#define HW_AudioAvcGain		"AVC gain"
#define HW_AudioAvcThresh	"AVC thresh"
#define HW_AudioAvcAttack	"AVC attack"
#define HW_AudioAvcDecay	"AVC decay"
#define HW_AudioI2S_Mux		"I2S Mux"
enum {I2S_ADC_IN, I2S_I2S_IN, I2S_Reserved, I2S_DAP};
#endif
*/
#define StdSampleRate 44100
#define BuffWaitFill 5
#ifdef PUBLICINTERFACE
#define WebPort 8000
#define UserPort 80
#else
#define WebPort 80
#endif

#define EASYSTREAMVERSION "2.000"

#define ICEVERSION "1.0030"

#define ConfigOffset1 0
#define ConfigOffset2 4096 
typedef enum {
	SCT_Manual, SCT_AudioLevel, SCT_Symetrix, SCT_SymetrixReport, SCT_Manual_IR
} StreamControlTypes;

typedef int SymCtrl;

#define StringsEqual(Const,String) memcmp(Const,String,sizeof(Const)-1) == 0
#define ConvertToPercent(v) (v * 100 / 32767)

extern volatile char OscBuff[2205];
extern volatile int OscBuffPtr;
extern volatile unsigned char * AudioBuffer;
extern volatile unsigned short AudioHeadPtr;
extern volatile unsigned short AudioTailPtr1;
extern volatile int StartError;
extern const char * HtmlPath;
extern const char * ConfigPath;
extern pthread_mutex_t namechangemutex;
extern pthread_mutex_t eepromconfigmutex;
//#define IS_DAEMON

#define PcmBufferSize 0x3FFFFF
#define PcmBufferBits 22
#define DebugBufferLines 60
extern volatile short * PcmBuffer;

#ifdef IS_DAEMON
extern pthread_mutex_t debugmutex;
#endif
 
typedef struct {
        int StreamActive; 
        int Ctrl_Stream;        
        int StreamConnected;
        int AuthError;        
        char * SourceName;
        char * UserName;
        char * Password;
        char * Name;
        char * URL;
        //int Public;
        int Port;
        //Private
        int RestartStream;
        int StreamConnectErr;
    } _Mount;

    typedef struct {
        StreamControlTypes ControlType;
        int BitRate;
        int EncType;
        int StreamOnLevel;
        int StreamOnDelay;
        _Mount Mounts[2];
    } _StreamSetup;

    typedef struct {
        int Type;
        DWORD_VAL IP;
        int Port;
        int TCP;
        SymCtrl Stream1;
        SymCtrl Stream2;
        SymCtrl StreamIn;
        SymCtrl AudioLevel;
        SymCtrl IsStreaming1;
        SymCtrl IsStreaming2;
        SymCtrl AudioPeak;
        SymCtrl RxSelection; //0-15
        int Error;
        int RestartNeeded;
    } _SymSettings;

enum {
	StrmErr_None, StrmErr_NoHost, StrmErr_NoConnect, StrmErr_NoWrite, StrmErr_Auth, StrmErr_LostConn
};

typedef struct {
        int HardwareGain;
        int SampleRate;
        double InVolume;
        int OutVolume;
        int AutoLimiter;
        //Private
        int NewInVolume;
        int NewOutVolume;
        int InInitErr;
        int InInitdone;
        int InitNeeded;
    } _HardwareSetup;
    
    typedef struct {
        char * Name;
        char * Username;
        char * Password;
        char * Url;
        //int Index;
        int Encoding;
        //Private
        int Active;
    }_RxStreams;

    typedef struct {
        //Public
        int Ctrl_StreamIn;
        int Ctrl_RxSelection;
        char PlayState[128];        
        _RxStreams RxStreams[32];
        //Private
        int StreamListenStatus;
        int RestartStreamIn;

    } _PlayerSetup;
    
    typedef struct {
        char * UpdateUrl;
        int DoUpdate;
        char * ControlUrl;
        int RemoteControl;
        char * PreAuthToken;
        int PollPeriod;
        char UpdateStatus[64];
		  char ControlStatus[64];
    } _Services;
    
    typedef struct {
        char * DeviceName;
        unsigned char DeviceSerial[9];
        unsigned char * AuthToken;
        unsigned long LastConfigStamp;
    }_Info;
    
    typedef struct {
        int RestartEncoder;
    } _Control;

typedef struct {
	char * Name;
	int value;
} AShelper;

typedef struct {
	pthread_t Thread;
	int session_fd;
	int Active;
} WebThreads;

typedef unsigned short EasyMixValue;
typedef unsigned short SymCtrlNum;
typedef union {

	struct {
		EasyMixValue Value;
		SymCtrlNum Control;
		int Error;
		char Type;
	};
	short SValue;
} _ConvertDspString;

    enum {
        EncType_MP3, EncType_AAC
    };

    enum {
        DT_STREAMER, DT_PLAYER
    };


extern volatile _Info Info;
//Streaming params
extern volatile _PlayerSetup PlayerSetup;
extern volatile _StreamSetup StreamSetup;
extern volatile _Services SysServices;
extern volatile _HardwareSetup HardwareSetup;
extern volatile _SymSettings DspSettings;
extern volatile _Control Control;

extern volatile int PeakDetector;
extern volatile int AudioLevel;
extern volatile int NewUrls;
extern const char * ConfigPath;
extern const char * BackupConfigPath;
extern volatile char * DeviceName;
extern volatile int BuffInMs;
extern void Debug(const char* format, ...);
extern int ConvertToDb10(int Value);
extern double time_diff(struct timeval x , struct timeval y);
extern int recv_catch_eintr(int socket, void * buf, size_t len, int flags, int timeout_ms);
extern int LimitInt(int val, int min, int max);
extern volatile char * DebugBuffer[DebugBufferLines];
#endif	/* GLOBAL_H */

