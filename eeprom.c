
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <pthread.h>
#include <errno.h>
#include "eeprom.h"

const char * SpiPath = "/dev/spidev32765.0";

unsigned char SPI1BUF, CS;
#define SPIGet()  SPI1BUF;
#define EEpromTimeout 25
#define PageSize 64
int EepromHandle = 0;

int EEPROMInit(void) {
	EepromHandle = open(SpiPath, O_RDWR);
	if (EepromHandle <= 0) {
		Debug("EEPROM handle null %i\n", errno);
		return 1;
	} else {
		return 0;
	}
}

void SPIPut(BYTE data) {
	SPI_TxRx(&data, &SPI1BUF, 1);
}

void SPI_TxRx(BYTE * TxData, BYTE * RxData, int Len) {

	struct spi_ioc_transfer xfer;
	unsigned char *bp;
	int status;
	if (EepromHandle <= 0) {
		Debug("EEPROM handle null\n");
		return;
	}
	memset(&xfer, 0, sizeof xfer);
	xfer.tx_buf = (unsigned long)TxData;
	xfer.rx_buf = (unsigned long)RxData;
	xfer.len = Len;
	status = ioctl(EepromHandle, SPI_IOC_MESSAGE(1), &xfer);
	if (status < 0) {
		Debug("EEPROM IO error %i\n",errno);
		return;
	}
}

void EEPROMWriteByte(BYTE data, WORD address) {
	int StatReads = 0;
	EEPROMWriteEnable();
	BYTE OutBuff[4] = {EEPROM_CMD_WRITE, ((WORD_VAL) address).v[1], ((WORD_VAL) address).v[0], data};
	SPI_TxRx(OutBuff, OutBuff, 4);
	// Wait for write end
	while (EEPROMReadStatus().Bits.WIP && StatReads++ < EEpromTimeout) {
		usleep(1000);
	}
}

BYTE EEPROMReadByte(WORD address) {
	BYTE OutBuff[4] = {EEPROM_CMD_READ, ((WORD_VAL) address).v[1], ((WORD_VAL) address).v[0], 0};
	SPI_TxRx(OutBuff, OutBuff, 4);
	return (OutBuff[3]);
}

void EEPROMWriteEnable(void)
{
    SPIPut(EEPROM_CMD_WREN);
}

union _EEPROMStatus_ EEPROMReadStatus(void) {
	BYTE OutBuff[2] = {EEPROM_CMD_RDSR, 0};
	BYTE InBuff[2];
	SPI_TxRx(OutBuff, InBuff, 2);
	return (union _EEPROMStatus_)InBuff[1];
}

int EEPROMWriteArray(DWORD address, void * pData, WORD nCount) {
	struct spi_ioc_transfer xfer[2];
	memset(xfer, 0, sizeof xfer);
	DWORD_VAL addr;
	BYTE OutBuff[3];
	int CountDown = nCount;
	int status;
	int len;
	volatile int wrcnt;
	addr.Val = address;
	char * charptr = (char*)pData;
	
	// WRITE

	while (CountDown > 0) {
		EEPROMWriteEnable();
		OutBuff[0] = EEPROM_CMD_WRITE;
		OutBuff[1] = addr.v[1];
		OutBuff[2] = addr.v[0];
		xfer[0].tx_buf = (unsigned long) OutBuff;
		xfer[0].rx_buf = (unsigned long) NULL;
		xfer[0].len = 3;

		xfer[1].tx_buf = (unsigned long) charptr;
		xfer[1].rx_buf = (unsigned long) NULL;
		int MaxLen = PageSize - addr.Val % PageSize;
		//Keep on page boundary
		if (CountDown > MaxLen) {
			len = MaxLen;
		} else {
			len = CountDown;
		}
		xfer[1].len = len;
		status = ioctl(EepromHandle, SPI_IOC_MESSAGE(2), &xfer);
		if (status <= 0) {
			return -1;
		} else {
		}
		charptr += len;
		addr.Val += len;
		CountDown -= len;
		wrcnt = 0;
		while (EEPROMReadStatus().Bits.WIP) {
			usleep(1000);
			if (wrcnt++>EEpromTimeout) {
				return -1;
			}
		}
	}

	// VERIFY
	/*
	for (counter = 0; counter < nCount; counter++) {
		if (*pData != EEPROMReadByte(address))
			return (0);
		pData++;
		address++;
	}
	 */
	return (1);
}

int EEPROMReadArray(WORD address, void * pData, WORD nCount) {
	int status;
	struct spi_ioc_transfer xfer[2];
	memset(xfer, 0, sizeof xfer);
	BYTE OutBuff[3] = {EEPROM_CMD_READ, ((WORD_VAL) address).v[1], ((WORD_VAL) address).v[0]};
	xfer[0].tx_buf = (unsigned long) OutBuff;
	xfer[0].rx_buf = (unsigned long) NULL;
	xfer[0].len = 3;

	xfer[1].tx_buf = (unsigned long) NULL;
	xfer[1].rx_buf = (unsigned long) pData;
	xfer[1].len = nCount;
	status = ioctl(EepromHandle, SPI_IOC_MESSAGE(2), &xfer);
	if (status < 0) {
		Debug("EEPROM IO error %i\n", errno);
		return -1;
	} else {
		return nCount;
	}
}