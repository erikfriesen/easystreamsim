/* 
 * File:   htmlhelpers..h
 * Author: Erik
 *
 * Created on September 12, 2015, 3:14 PM
 */

#ifndef HTMLHELPERS__H
#define	HTMLHELPERS__H

#ifdef	__cplusplus
extern "C" {
#endif

int GetHeader(char * buff, char * response, char * ctype);
int Get404(char * buff,char * url);

#ifdef	__cplusplus
}
#endif

#endif	/* HTMLHELPERS__H */

