#include <time.h>
#include <string.h>
#include <stdio.h>
#define StringsEqual(Const,String) memcmp(Const,String,sizeof(Const)-1) == 0

int GetHeader(char * buff, char * response, char * ctype ) {
	char * bstart = buff;
	char * charset = "utf-8";
	time_t now = time(0);
	struct tm tm = *gmtime(&now);
	buff += sprintf(buff, "HTTP/1.1 %s\r\n", response);
	buff += strftime(buff, 50, "Date: %a, %d %b %Y %H:%M:%S %Z\r\n", &tm);
	if (StringsEqual("text/event-stream", ctype)) {
		charset = "utf-8";
	} else if (StringsEqual("application/octet-stream", ctype)) {
		charset = "Windows-1252";
	}
	buff += sprintf(buff, "Content-Type: %s; charset=%s\r\n", ctype, charset);
	buff += sprintf(buff, "Server: IceStreamer\r\n\r\n");
	return buff - bstart;
}

int Get404(char * buff, char * url) {
	return sprintf(buff,
			  "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\r\n"
			  "<html><head>\r\n"
			  "<title>404 Not Found</title>\r\n"
			  "</head><body>\r\n"
			  "<h1>Not Found</h1>\r\n"
			  "<p>The requested URL /%s was not found on this server.</p>\r\n"
			  "<hr>\r\n"
			  "</body></html>\r\n", url);
}

