/* 
 * File:   configsave.h
 * Author: Erik
 *
 * Created on January 27, 2016, 1:25 PM
 */

#ifndef CONFIGSAVE_H
#define	CONFIGSAVE_H

#define MaxConfigLen 4080

char * BuildXmlStatus(int Complete);
char * BuildXmlConfig(void);
int SaveCurrentConfig(void);
int ConfigManager(void);
int ReadMainConfig(char * buff, int maxlen);
int ReadBackupConfig(char * buff, int maxlen);
void FillConfigInfo(json_value* value);
int SaveNewConfig(void);
#ifdef USE_USB_STORAGE
int ReadOnlyBackupConfig(char * buff, int maxlen);
#endif

#endif	/* CONFIGSAVE_H */

