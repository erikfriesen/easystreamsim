#include <errno.h>
#include <pthread.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <math.h>
#include "include/GenericTypeDefs.h"
#include "global.h"
#include "iceconfig.h"
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include "json.h"
#include "configsave.h"

static int ParseJsonConfig(json_value* value);
static void ManagePlayerParams(_PlayerSetup ps);
static void ManageDspSettings(_SymSettings s);
static void ManageHardwareParams(_HardwareSetup hs);
static void ManageStreamParams(_StreamSetup ss);
static void ControlTasks(void);
static int sock;

enum {
	rm_WaitStart, rm_Dns, rm_Connect_Wait, rm_Open_Sock, rm_Open_Send, rm_Rx_Response, rm_ParseJson, rm_Provision, rm_Provision_Send, rm_Provision_Rx, rm_Provision_Fail, rm_Err, rm_Delay
};
static int RemTask = rm_WaitStart;
static struct sockaddr_in si_other;
static unsigned char inputBuffer[4096];
static unsigned char rxbuff[32767];
static unsigned char tbuff[4096];
static int SockErr;
static int RetryDelay = 30;
static time_t timer, LastData, comparetime;
static int SslFlag = 0;
static char * urlstart;
static char * POST = "";
static struct timeval tvs, tvd;
static int RxLocation = 0;
static int Port = 80;
static unsigned long LastRxUnixTime = 0;
static int FirstOutInfo = 1;
static int ReProvision = 0;
static json_value* jvalue;
#define timeout_ms 5000

static char * strdup_max64(char * str) {
	int len = strlen(str);
	if (len > 64) {
		len = 64;
	}
	char * ret = malloc(len + 1);
	memcpy(ret, str, len + 1);
	return ret;
}

char * BuildProvisionRequest(void) {
	char * buff = malloc(2048);
	if (buff == NULL) {
		return NULL;
	}
	char * SPtr = buff;
	SPtr += sprintf(SPtr, "{\"Provision\":{");

	SPtr += sprintf(SPtr, "\"DeviceSerial\":\"%c%c%c:%c%c%c:%c%c%c\",",
			  Info.DeviceSerial[8], Info.DeviceSerial[7], Info.DeviceSerial[6],
			  Info.DeviceSerial[5], Info.DeviceSerial[4], Info.DeviceSerial[3],
			  Info.DeviceSerial[2], Info.DeviceSerial[1], Info.DeviceSerial[0]);
	SPtr += sprintf(SPtr, "\"AuthToken\":\"%s\"}}", SysServices.PreAuthToken);
	return buff;
}

static int CompareId(char * DevId) {
	int a;
	for (a = 8; a >= 0; a--) {
		if (*DevId == ':') {
			DevId++;
		}
		if (Info.DeviceSerial[a] != *DevId++) {
			return 0;
		}
	}
	return 1;
}

void * RemControlThread(void * ptr) {
	MyLog("Started Remote control thread\n");
	while (1) {
		usleep(1000); //Give threads a breather
		ControlTasks();
	}
}

static void ControlTasks(void) {

	int a;
	char * ptr;
	struct timeval tv = {.tv_sec = 10, .tv_usec = 1};
	char * out;
	switch (RemTask) {
		case rm_WaitStart:
			if (SysServices.RemoteControl){
				RemTask = rm_Dns;
			}
			break;
		case rm_Dns:
			sprintf(inputBuffer, "%s", SysServices.ControlUrl);
			SslFlag = 0;
			urlstart = strstr((const unsigned char*) inputBuffer, "https://");
			if (urlstart == NULL) {
				urlstart = strstr((const unsigned char*) inputBuffer, "http://");
			} else {
				urlstart += 8;
				SslFlag = 1;
			}
			if (urlstart == NULL) {
				urlstart = inputBuffer;
			} else {
				urlstart += 7;
			}
			Port = 80;
			char * slash = strchr(urlstart, '/');
			POST = "";
			if (slash != NULL) {
				*slash++ = 0;
				POST = slash;
			}
			char * colon = strchr(urlstart, ':');
			if (colon != NULL) {
				//Extract port
				*colon++ = 0;
				Port = strtol(colon, NULL, 10);
				if (Port == 0 || Port > 65535) {
					if (SockErr != 1) {
						sprintf((char*) SysServices.ControlStatus, "Malformed URL or port number");
						Debug("Couldn't parse port number\n");
						SockErr = 1;
						RemTask = rm_Err;
						RetryDelay = 30;
						return;
					}
				}
			}

			si_other.sin_family = AF_INET;
			si_other.sin_port = htons(Port);

			struct hostent *lh = gethostbyname(urlstart);
			if (lh == NULL) {
				if (SockErr != 2) {
					sprintf((char*) SysServices.ControlStatus, "Unable to find host");
					Debug("PB: Cannot find host : %s\n", strerror(errno));
					SockErr = 2;
				}
				RemTask = rm_Err;
				RetryDelay = 30;
				return;
			} 
			time(&timer);
			RemTask = rm_Connect_Wait;
			bcopy((char *) lh->h_addr, (char *) &si_other.sin_addr.s_addr, lh->h_length);
			if (strlen(Info.AuthToken) < 5 || ReProvision) {
				RemTask = rm_Provision;
				ReProvision = 0;
			}
			break;
		case rm_Connect_Wait:
			if (!SysServices.RemoteControl) {
				RemTask = rm_WaitStart;
			}
			time(&comparetime);
			if (comparetime - timer >= SysServices.PollPeriod) {
				time(&timer);
				RemTask = rm_Open_Sock;
				
			}
			break;
		case rm_Open_Sock:
			sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (sock < 0) {
					if (SockErr != 3) {
					Debug("PB: Cannot open socket %s\n", strerror(errno));
					SockErr = 3;
				}
				RemTask = rm_Err;
				RetryDelay = 30;
			}
			if (connect(sock, (__CONST_SOCKADDR_ARG) & si_other, sizeof (si_other)) < 0) {
				if (SockErr != 4) {
					Debug("PB: Cannot connect socket URL %s-Port %i-GET %s-ERROR %s\n", inputBuffer, Port, POST, strerror(errno));
					SockErr = 4;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Err;
				return;
			}

			if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof (tv))) {
				if (SockErr != 5) {
					Debug("PB: Cannot set socket SO_RCVTIMEO %s\n", strerror(errno));
					SockErr = 5;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Err;
				return;
			}
			if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof (tv))) {
				if (SockErr != 6) {
					Debug("PB: Cannot set socket SO_SNDTIMEO %s\n", strerror(errno));
					SockErr = 6;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Err;
				return;
			}
			RemTask = rm_Open_Send;
			break;
		case rm_Open_Send:
			out = tbuff;
			char * outinfo;
			if (FirstOutInfo || LastRxUnixTime < Info.LastConfigStamp) {
				outinfo = BuildXmlConfig();
				FirstOutInfo = 0;
			} else {
				outinfo = BuildXmlStatus(1);
			}
			out += sprintf(out, "POST /%s HTTP/1.0\r\n", POST);
			out += sprintf(out, "Host: %s\r\n", urlstart);
			out += sprintf(out, "Content-Length: %i\r\n", strlen(outinfo));
			out += sprintf(out, "Cache-Control: no-cache\r\n");
			out += sprintf(out, "User-Agent: EasyStream\r\n");
			out += sprintf(out, "Content-Type: text/plain;charset=UTF-8\r\n");
			out += sprintf(out, "Accept: */*\r\n");
			out += sprintf(out, "Accept-Encoding: identity;q=1, *;q=0\r\n");
			out += sprintf(out, "Referer: %s/%s\r\n\r\n%s", inputBuffer, POST, outinfo);
			free(outinfo);

			timer = time(0);
			if (sendto(sock, tbuff, strlen(tbuff), MSG_NOSIGNAL, (struct sockaddr *) &si_other, sizeof (si_other)) < 0) {
				if (SockErr != 7) {
					Debug("Couldn't send http header %s\n", strerror(errno));
					sprintf((char*) SysServices.ControlStatus, "Unable to send header");
					SockErr = 7;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Err;
				return;
			}
			//Debug(tbuff);
			RemTask = rm_Rx_Response;
			gettimeofday(&tvs, NULL);
			RxLocation = 0;
			break;
		case rm_Rx_Response:
			a = recv(sock, &rxbuff[RxLocation], sizeof (rxbuff) - RxLocation, MSG_NOSIGNAL);
			if (a > 0) {
				RxLocation += a;
				gettimeofday(&tvs, NULL);
				char * LengthPointer = strstr(rxbuff, "Content-Length: ");
				char * StartPointer = strstr(rxbuff, "\r\n\r\n");
				if (LengthPointer != NULL && StartPointer != NULL) {
					StartPointer += 4;
					LengthPointer += 16;
					int ContentLength = strtol(LengthPointer, NULL, 10);
					int ActualLength = &rxbuff[RxLocation] - (unsigned char*) StartPointer;
					if (ActualLength == ContentLength) {
						//Parse data here
						rxbuff[RxLocation] = 0;
						//Debug("RX[%s]\n", StartPointer);//Uncomment this line to view incoming raw json
						jvalue = json_parse(StartPointer, ActualLength);
						if (jvalue == NULL) {
							if (SockErr != 8) {
								Debug("Bad request returned%s\n", strerror(errno));
								sprintf((char*) SysServices.ControlStatus, "Bad control response");
								SockErr = 8;
								RemTask = rm_Provision_Fail;
								close(sock);
							}
						} else {
							RemTask = rm_ParseJson;
							close(sock);
						}
					}
				}
			} else if (a < 0 && errno != EINTR) {
				if (SockErr != 10) {
					Debug("Couldn't receive at provision rx%s\n", strerror(errno));
					sprintf((char*) SysServices.ControlStatus, "Unable to receive data");
					SockErr = 10;
				}
				close(sock);
				RemTask = rm_Provision_Fail;
			} else {
				gettimeofday(&tvd, NULL);
				if (time_diff(tvd, tvs) > timeout_ms * 1000) {
					close(sock);
					RemTask = rm_Provision_Fail;
				}
			}
			break;
		case rm_ParseJson:
			//Is it an ack, or new data? Are we authorized?
			ptr = json_find_string(jvalue, "Ack.Auth");
			if (strcmp("false", ptr) == 0) {
				ReProvision = 1;
				RemTask = rm_WaitStart;
				sprintf((char*) SysServices.ControlStatus, "Unauthorized");
				return;
			}
			if (strcmp("true", ptr) == 0) {
				//Regular ack
				ptr = json_find_string(jvalue, "Ack.DeviceSerial");
				if (ptr == NULL || !CompareId(ptr)) {
					if (SockErr != 10) {
						Debug("Device id doesn't match at ack\n");
						sprintf((char*) SysServices.ControlStatus, "Bad response id");
						SockErr = 10;
					}
					RemTask = rm_Dns;
					return;
				}
				RemTask = rm_Dns;
				LastRxUnixTime = json_find_int(jvalue, "Ack.Unixtime");
				sprintf((char*) SysServices.ControlStatus, "Polled %i", time(NULL));
			} else {
				ptr = json_find_string(jvalue, "Config.Info.DeviceSerial");
				if(ptr == NULL){
					Debug("ptr = NULL\n");
				}
				if (ptr == NULL || !CompareId(ptr)) {
					if (SockErr != 10) {
						Debug("Device id doesn't match at info\n");
						sprintf((char*) SysServices.ControlStatus, "Bad response id");
						SockErr = 10;
					}
					RemTask = rm_Dns;
					return;
				}
				LastRxUnixTime = json_find_int(jvalue, "Config.Info.Unixtime");
				if (LastRxUnixTime > Info.LastConfigStamp) {
					ParseJsonConfig(jvalue);
				}
				sprintf((char*) SysServices.ControlStatus, "Polled %i", time(NULL));
				RemTask = rm_Dns;
			}
			break;
		case rm_Provision:
			sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
			if (sock < 0) {
					if (SockErr != 3) {
					Debug("PB: Cannot open socket %s\n", strerror(errno));
					SockErr = 3;
				}
				RemTask = rm_Err;
				RetryDelay = 30;
			}
			Debug("Provision Socket %i opened\n", sock);
			if (connect(sock, (__CONST_SOCKADDR_ARG) & si_other, sizeof (si_other)) < 0) {
				if (SockErr != 4) {
					Debug("PB: Cannot connect socket URL %s-Port %i-GET %s-ERROR %s\n", inputBuffer, Port, POST, strerror(errno));
					SockErr = 4;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Provision_Fail;
				return;
			}

			if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof (tv))) {
				if (SockErr != 5) {
					Debug("PB: Cannot set socket SO_RCVTIMEO %s\n", strerror(errno));
					SockErr = 5;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Provision_Fail;
				return;
			}
			if (setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof (tv))) {
				if (SockErr != 6) {
					Debug("PB: Cannot set socket SO_SNDTIMEO %s\n", strerror(errno));
					SockErr = 6;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Provision_Fail;
				return;
			}
			RemTask = rm_Provision_Send;
			break;
		case rm_Provision_Send:
			out = tbuff;
			char * req = BuildProvisionRequest();
			out += sprintf(out, "POST /%s HTTP/1.0\r\n", POST);
			out += sprintf(out, "Host: %s\r\n", urlstart);
			out += sprintf(out, "Content-Length: %i\r\n", strlen(req));
			out += sprintf(out, "Cache-Control: no-cache\r\n");
			out += sprintf(out, "User-Agent: EasyStream\r\n");
			out += sprintf(out, "Content-Type: text/plain;charset=UTF-8\r\n");
			out += sprintf(out, "Accept: */*\r\n");
			out += sprintf(out, "Accept-Encoding: identity;q=1, *;q=0\r\n");
			out += sprintf(out, "Referer: %s/%s\r\n\r\n%s", inputBuffer, POST, req);
			free(req);
			
			timer = time(0);
			if (sendto(sock, tbuff, strlen(tbuff), MSG_NOSIGNAL, (struct sockaddr *) &si_other, sizeof (si_other)) < 0) {
				if (SockErr != 7) {
					Debug("Couldn't send http header %s\n", strerror(errno));
					sprintf((char*) SysServices.ControlStatus, "Unable to send header");
					SockErr = 7;
				}
				close(sock);
				sock = -1;
				RemTask = rm_Provision_Fail;
				return;
			}
			//Debug(tbuff);
			RemTask = rm_Provision_Rx;
			gettimeofday(&tvs, NULL);
			RxLocation = 0;
			break;
		case rm_Provision_Rx:
			a = recv(sock, &rxbuff[RxLocation], sizeof (rxbuff) - RxLocation, MSG_NOSIGNAL);
			if (a > 0) {
				RxLocation += a;
				gettimeofday(&tvs, NULL);
				char * LengthPointer = strstr(rxbuff, "Content-Length: ");
				char * StartPointer = strstr(rxbuff, "\r\n\r\n");
				if (LengthPointer != NULL && StartPointer != NULL) {
					StartPointer += 4;
					LengthPointer += 16;
					int ContentLength = strtol(LengthPointer, NULL, 10);
					int ActualLength = &rxbuff[RxLocation] - (unsigned char*)StartPointer;
					if (ActualLength == ContentLength) {
						//Parse data here
						Debug(StartPointer);
						jvalue = json_parse(StartPointer, ActualLength);
						if (jvalue == NULL) {
							if (SockErr != 8) {
								Debug("Bad request returned%s\n", strerror(errno));
								sprintf((char*) SysServices.ControlStatus, "Bad provision response");
								SockErr = 8;
								RemTask = rm_Provision_Fail;
							}
						} else {
							char * DevId = json_find_string(jvalue, "ProvisionResponse.DeviceSerial");
							char * AuthToken = json_find_string(jvalue, "ProvisionResponse.AuthToken");
							if (AuthToken == NULL || DevId == NULL) {
								if (SockErr != 9) {
									Debug("Missing Json in request%s\n", strerror(errno));
									sprintf((char*) SysServices.ControlStatus, "Bad json");
									SockErr = 9;
									RemTask = rm_Provision_Fail;
								}
							} else {
								if (!CompareId(DevId)) {
									RemTask = rm_Provision_Fail;
									if (SockErr != 10) {
										Debug("Device id doesn't match at provision\n");
										sprintf((char*) SysServices.ControlStatus, "Bad response id");
										SockErr = 10;
									}
									return;
								}
								if (Info.AuthToken != NULL) {
									free(Info.AuthToken);
								}
								Info.AuthToken = strdup(AuthToken);
								sprintf((char*) SysServices.ControlStatus, "Provisioned");
								RemTask = rm_Connect_Wait;
								time(&timer);
								SaveCurrentConfig();
								close(sock);
							}
						}
					}
				}
			} else if (a < 0 && errno != EINTR) {
				if (SockErr != 10) {
					Debug("Couldn't receive at provision rx%s\n", strerror(errno));
					sprintf((char*) SysServices.ControlStatus, "Unable to receive data");
					SockErr = 10;
				}
				close(sock);
				RemTask = rm_Provision_Fail;
			} else {
				gettimeofday(&tvd, NULL);
				if (time_diff(tvd, tvs) > timeout_ms * 1000) {
					close(sock);
					RemTask = rm_Provision_Fail;
				}
			}
			break;
		case rm_Provision_Fail:
			if (SockErr != 20) {
				sprintf((char*) SysServices.ControlStatus, "Unable to provision");
				Debug("Unable to provision\n");
				SockErr = 20;
			}
			RetryDelay = 60;
			break;
		case rm_Err:
			time(&timer);
			RemTask = rm_Delay;
			break;
		case rm_Delay:
			time(&comparetime);
			if (comparetime - timer >= RetryDelay) {
				RemTask = rm_WaitStart;
			}
			break;
	}
}

static int ParseJsonConfig(json_value * value) {

	//Split into strings
	if (value == NULL) {
		//strcpy(json, "Invalid json\n");
		Debug("Invalid json POST\r\n");
		goto badfinish;
	}
	int a;
	char hb[256];
	//_Control conset = {0};
	//_Info infoset = {0};
	_PlayerSetup playset = {0};
	_StreamSetup streamset = {0};
	_Services firmset = {0};
	_HardwareSetup hardset = {0};
	_SymSettings dspset = {0};
	//Info
	char * Unlock = json_find_string(value, "Config.Info.RenameUnlock");
	if (memcmp(Unlock, "true", 4) == 0) {
		free((char*) Info.DeviceName);
		Info.DeviceName = strdup(json_find_string(value, "Config.Info.DeviceName"));
	}
	Unlock = json_find_string(value, "Config.Info.DevIdUnlock");
	if (memcmp(Unlock, "true", 4) == 0) {
		char * DevId = json_find_string(value, "Config.Info.DeviceSerial");
		while (*DevId && a < 10) {
			Info.DeviceSerial[a++] = *DevId++;
			if (*DevId == ':') {
				DevId++;
			}
		}
	}
	Unlock = json_find_string(value, "Config.Info.AuthTokenUnlock");
	if (memcmp(Unlock, "true", 4) == 0) {
		free((char*) Info.AuthToken);
		Info.AuthToken = strdup(json_find_string(value, "Config.Info.AuthToken"));
	}
	Info.LastConfigStamp = json_find_int(value, "Config.Info.Unixtime");
	//Player
	a = json_find_int(value, "Config.Player.CtrlRxSelection");
	if (a != PlayerSetup.Ctrl_RxSelection) {
		PlayerSetup.Ctrl_RxSelection = a;
		PlayerSetup.RestartStreamIn = 1;
	}
	PlayerSetup.Ctrl_StreamIn = json_find_int(value, "Config.Player.CtrlStreamIn");
	//****  Further player setup
	for (a = 0; a < 32; a++) {
		sprintf(hb, "Config.Player.RxStreams[%i].Name", a);
		playset.RxStreams[a].Name = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Username", a);
		playset.RxStreams[a].Username = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Password", a);
		playset.RxStreams[a].Password = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Url", a);
		playset.RxStreams[a].Url = json_find_string(value, hb);
		sprintf(hb, "Config.Player.RxStreams[%i].Encoding", a);
		playset.RxStreams[a].Encoding = strcmp(json_find_string(value, hb), "AAC") == 0 ? EncType_AAC : EncType_MP3;
		playset.RxStreams[a].Active = strlen(playset.RxStreams[a].Url) > 0;
	}
	//Stream
	StreamSetup.ControlType = json_find_int(value, "Config.Stream.ControlType");
	StreamSetup.Mounts[0].Ctrl_Stream = json_find_int(value, "Config.Stream.Mounts[0].CtrlStream");
	StreamSetup.Mounts[1].Ctrl_Stream = json_find_int(value, "Config.Stream.Mounts[1].CtrlStream");
	streamset.BitRate = json_find_int(value, "Config.Stream.BitRate");
	streamset.EncType = json_find_int(value, "Config.Stream.EncType");
	StreamSetup.StreamOnLevel = json_find_int(value, "Config.Stream.StreamOnLevel");
	StreamSetup.StreamOnDelay = json_find_int(value, "Config.Stream.StreamOnDelay");
	streamset.Mounts[0].SourceName = json_find_string(value, "Config.Stream.Mounts[0].SourceName");
	streamset.Mounts[0].UserName = json_find_string(value, "Config.Stream.Mounts[0].UserName");
	streamset.Mounts[0].Password = json_find_string(value, "Config.Stream.Mounts[0].Password");
	streamset.Mounts[0].Name = json_find_string(value, "Config.Stream.Mounts[0].Name");
	streamset.Mounts[0].URL = json_find_string(value, "Config.Stream.Mounts[0].URL");
	streamset.Mounts[0].Port = json_find_int(value, "Config.Stream.Mounts[0].Port");
	streamset.Mounts[0].StreamActive = json_find_int(value, "Config.Stream.Mounts[0].StreamActive");
	streamset.Mounts[1].SourceName = json_find_string(value, "Config.Stream.Mounts[1].SourceName");
	streamset.Mounts[1].UserName = json_find_string(value, "Config.Stream.Mounts[1].UserName");
	streamset.Mounts[1].Password = json_find_string(value, "Config.Stream.Mounts[1].Password");
	streamset.Mounts[1].Name = json_find_string(value, "Config.Stream.Mounts[1].Name");
	streamset.Mounts[1].URL = json_find_string(value, "Config.Stream.Mounts[1].URL");
	streamset.Mounts[1].Port = json_find_int(value, "Config.Stream.Mounts[1].Port");
	streamset.Mounts[1].StreamActive = json_find_int(value, "Config.Stream.Mounts[1].StreamActive");
	//Firmware
	firmset.UpdateUrl = json_find_string(value, "Config.Services.UpdateUrl");
	if (strcmp(firmset.UpdateUrl, SysServices.UpdateUrl) != 0) {
		free(SysServices.UpdateUrl);
		SysServices.UpdateUrl = strdup_max64(firmset.UpdateUrl);
	}
	SysServices.DoUpdate = json_find_int(value, "Config.Services.DoUpdate");

	firmset.ControlUrl = json_find_string(value, "Config.Services.ControlUrl");
	if (strcmp(firmset.ControlUrl, SysServices.ControlUrl) != 0) {
		free(SysServices.ControlUrl);
		SysServices.ControlUrl = strdup_max64(firmset.ControlUrl);
	}

	firmset.PreAuthToken = json_find_string(value, "Config.Services.PreAuthToken");
	if (strcmp(firmset.PreAuthToken, SysServices.PreAuthToken) != 0) {
		free(SysServices.PreAuthToken);
		SysServices.PreAuthToken = strdup(firmset.PreAuthToken);
	}
	SysServices.PollPeriod = json_find_int(value, "Config.Services.PollPeriod");
	//Hardware
	hardset.AutoLimiter = json_find_int(value, "Config.Hardware.AutoLimiter");
	hardset.HardwareGain = json_find_int(value, "Config.Hardware.HardwareGain");
	hardset.SampleRate = json_find_int(value, "Config.Hardware.SampleRate");
	hardset.InVolume = json_find_double(value, "Config.Hardware.InVolume");
	hardset.OutVolume = json_find_int(value, "Config.Hardware.OutVolume");
	//DSP
	char * SymIp = json_find_string(value, "Config.DSP.IP");
	char * pch = strtok(SymIp, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[0] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[1] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[2] = strtol(pch, NULL, 10);

	pch = strtok(NULL, ".");
	if (pch == NULL) {
		goto badfinish;
	}
	dspset.IP.v[3] = strtol(pch, NULL, 10);

	dspset.Port = json_find_int(value, "Config.DSP.Port");
	dspset.TCP = json_find_int(value, "Config.DSP.TCP");
	dspset.RxSelection = json_find_int(value, "Config.DSP.RxSelection");
	DspSettings.Stream1 = json_find_int(value, "Config.DSP.Stream1");
	DspSettings.Stream2 = json_find_int(value, "Config.DSP.Stream2");
	DspSettings.StreamIn = json_find_int(value, "Config.DSP.StreamIn");
	DspSettings.IsStreaming1 = json_find_int(value, "Config.DSP.IsStreaming1");
	DspSettings.IsStreaming2 = json_find_int(value, "Config.DSP.IsStreaming2");
	DspSettings.AudioPeak = json_find_int(value, "Config.DSP.AudioPeak");
	DspSettings.AudioLevel = json_find_int(value, "Config.DSP.AudioLevel");
	//Validate

	ManagePlayerParams(playset);
	ManageStreamParams(streamset);
	ManageHardwareParams(hardset);
	ManageDspSettings(dspset);
	json_value_free(value);
	//Save config files
	SaveCurrentConfig();
	Debug("ParseJsonConfig success\r\n");
	return 0;
badfinish:
	Debug("ParseJsonConfig error\r\r\n");
	return 1;
}

static void ManagePlayerParams(_PlayerSetup ps) {
	int a, i;
	int RestartStreamIn = 0;
	i = PlayerSetup.Ctrl_RxSelection - 1;
	if (i < 0 || i > 31) {
		i = 0;
	}
	for (a = 0; a < 32; a++) {
		if (strcmp(ps.RxStreams[a].Name, PlayerSetup.RxStreams[a].Name) != 0) {
			free(PlayerSetup.RxStreams[a].Name);
			PlayerSetup.RxStreams[a].Name = strdup_max64(ps.RxStreams[a].Name);
			RestartStreamIn = 1;
		}
		if (strcmp(ps.RxStreams[a].Username, PlayerSetup.RxStreams[a].Username) != 0) {
			free(PlayerSetup.RxStreams[a].Username);
			PlayerSetup.RxStreams[a].Username = strdup_max64(ps.RxStreams[a].Username);
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (strcmp(ps.RxStreams[a].Password, PlayerSetup.RxStreams[a].Password) != 0) {
			free(PlayerSetup.RxStreams[a].Password);
			PlayerSetup.RxStreams[a].Password = strdup_max64(ps.RxStreams[a].Password);
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (strcmp(ps.RxStreams[a].Url, PlayerSetup.RxStreams[a].Url) != 0) {
			free(PlayerSetup.RxStreams[a].Url);
			PlayerSetup.RxStreams[a].Url = strdup_max64(ps.RxStreams[a].Url);
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (ps.RxStreams[a].Encoding != PlayerSetup.RxStreams[a].Encoding) {
			PlayerSetup.RxStreams[a].Encoding = ps.RxStreams[a].Encoding;
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
		if (ps.RxStreams[a].Active != PlayerSetup.RxStreams[a].Active) {
			PlayerSetup.RxStreams[a].Active = ps.RxStreams[a].Active;
			if (a == i) {
				RestartStreamIn = 1;
			}
		}
	}
	if (RestartStreamIn) {
		PlayerSetup.RestartStreamIn = 1;
	}
}

static void ManageStreamParams(_StreamSetup ss) {

	int Restart1 = 0;
	int Restart2 = 0;
	int Restart = 0;
	if (ss.BitRate != StreamSetup.BitRate) {
		StreamSetup.BitRate = ss.BitRate;
		Restart = 1;
	}
	if (ss.EncType != StreamSetup.EncType) {
		StreamSetup.EncType = ss.EncType;
		Restart = 1;
	}
	if (Restart) {
		Control.RestartEncoder = 1;
		StreamSetup.Mounts[0].RestartStream = 1;
		StreamSetup.Mounts[1].RestartStream = 1;
		Debug("Restarting Encoder\r\n");
	}

	if (strcmp(ss.Mounts[0].SourceName, StreamSetup.Mounts[0].SourceName) != 0) {
		free(StreamSetup.Mounts[0].SourceName);
		StreamSetup.Mounts[0].SourceName = strdup_max64(ss.Mounts[0].SourceName);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].UserName, StreamSetup.Mounts[0].UserName) != 0) {
		free(StreamSetup.Mounts[0].UserName);
		StreamSetup.Mounts[0].UserName = strdup_max64(ss.Mounts[0].UserName);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].Password, StreamSetup.Mounts[0].Password) != 0) {
		free(StreamSetup.Mounts[0].Password);
		StreamSetup.Mounts[0].Password = strdup_max64(ss.Mounts[0].Password);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].Name, StreamSetup.Mounts[0].Name) != 0) {
		free(StreamSetup.Mounts[0].Name);
		StreamSetup.Mounts[0].Name = strdup_max64(ss.Mounts[0].Name);
		Restart1 = 1;
	}
	if (strcmp(ss.Mounts[0].URL, StreamSetup.Mounts[0].URL) != 0) {
		free(StreamSetup.Mounts[0].URL);
		StreamSetup.Mounts[0].URL = strdup_max64(ss.Mounts[0].URL);
		Restart1 = 1;
	}

	if (ss.Mounts[0].Port != StreamSetup.Mounts[0].Port) {
		StreamSetup.Mounts[0].Port = ss.Mounts[0].Port;
		Restart1 = 1;
	}

	if (ss.Mounts[0].StreamActive != StreamSetup.Mounts[0].StreamActive) {
		StreamSetup.Mounts[0].StreamActive = ss.Mounts[0].StreamActive;
		Restart1 = 1;
	}

	if (Restart1) {
		StreamSetup.Mounts[0].RestartStream = 1;
		StreamSetup.Mounts[0].AuthError = 0;
		Debug("Restarting Stream 1\r\n");
	}

	if (strcmp(ss.Mounts[1].SourceName, StreamSetup.Mounts[1].SourceName) != 0) {
		free(StreamSetup.Mounts[1].SourceName);
		StreamSetup.Mounts[1].SourceName = strdup_max64(ss.Mounts[1].SourceName);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].UserName, StreamSetup.Mounts[1].UserName) != 0) {
		free(StreamSetup.Mounts[1].UserName);
		StreamSetup.Mounts[1].UserName = strdup_max64(ss.Mounts[1].UserName);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].Password, StreamSetup.Mounts[1].Password) != 0) {
		free(StreamSetup.Mounts[1].Password);
		StreamSetup.Mounts[1].Password = strdup_max64(ss.Mounts[1].Password);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].Name, StreamSetup.Mounts[1].Name) != 0) {
		free(StreamSetup.Mounts[1].Name);
		StreamSetup.Mounts[1].Name = strdup_max64(ss.Mounts[1].Name);
		Restart2 = 1;
	}
	if (strcmp(ss.Mounts[1].URL, StreamSetup.Mounts[1].URL) != 0) {
		free(StreamSetup.Mounts[1].URL);
		StreamSetup.Mounts[1].URL = strdup_max64(ss.Mounts[1].URL);
		Restart2 = 1;
	}

	if (ss.Mounts[1].Port != StreamSetup.Mounts[1].Port) {
		StreamSetup.Mounts[1].Port = ss.Mounts[1].Port;
		Restart2 = 1;
	}
	if (ss.Mounts[1].StreamActive != StreamSetup.Mounts[1].StreamActive) {
		StreamSetup.Mounts[1].StreamActive = ss.Mounts[1].StreamActive;
		Restart1 = 1;
	}
	if (Restart2) {
		StreamSetup.Mounts[1].RestartStream = 1;
		StreamSetup.Mounts[1].AuthError = 0;
		Debug("Restarting Stream 2\r\n");
	}
}

static void ManageHardwareParams(_HardwareSetup hs) {
	int Restart = 0;
	if (hs.HardwareGain != HardwareSetup.HardwareGain) {
		HardwareSetup.HardwareGain = hs.HardwareGain;
		Restart = 1;
	}
	if (hs.SampleRate != HardwareSetup.SampleRate) {
		HardwareSetup.SampleRate = hs.SampleRate;
		Restart = 1;
	}
	if (hs.InVolume != HardwareSetup.InVolume) {
		HardwareSetup.InVolume = hs.InVolume;
		Restart = 1;
	}
	if (hs.OutVolume != HardwareSetup.OutVolume) {
		HardwareSetup.OutVolume = hs.OutVolume;
		if (HardwareSetup.OutVolume<-48) {
			HardwareSetup.OutVolume = -48;
		} else if (HardwareSetup.OutVolume > 0) {
			HardwareSetup.OutVolume = 0;
		}
		Restart = 1;
	}
	if (hs.AutoLimiter != HardwareSetup.AutoLimiter) {
		HardwareSetup.AutoLimiter = hs.AutoLimiter;
	}
	if (Restart) {
		HardwareSetup.InInitErr = 0;
		HardwareSetup.InInitdone = 0;
		HardwareSetup.InitNeeded = 1;
		Debug("Restarting Audio\r\n");
	}
}

static void ManageDspSettings(_SymSettings s) {

	int RestartStreamIn = 0;
	int RestartNeeded = 0;
	if (s.IP.Val != DspSettings.IP.Val) {
		DspSettings.IP.Val = s.IP.Val;
		RestartNeeded = 1;
	}
	if (s.Port != DspSettings.Port) {
		DspSettings.Port = s.Port;
		RestartNeeded = 1;
	}
	if (s.TCP != DspSettings.TCP) {
		DspSettings.TCP = s.TCP;
		RestartNeeded = 1;
	}

	if (s.RxSelection != DspSettings.RxSelection) {
		DspSettings.RxSelection = s.RxSelection;
		RestartStreamIn = 1;
	}

	if (RestartStreamIn) {
		PlayerSetup.RestartStreamIn = 1;
	}
	if (RestartNeeded) {
		DspSettings.RestartNeeded = 1;
		Debug("Restarting Symetrix\r\n");
	}
}
