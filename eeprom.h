/* 
 * File:   eeprom.h
 * Author: Erik
 *
 * Created on January 29, 2016, 1:46 PM
 */

#ifndef EEPROM_H
#define	EEPROM_H

#include "include/GenericTypeDefs.h"

#define EEPROM_PAGE_SIZE    (unsigned)64
#define EEPROM_PAGE_MASK    (unsigned)0x003f
#define EEPROM_CMD_READ     (unsigned)0x03
#define EEPROM_CMD_WRITE    (unsigned)0x02
#define EEPROM_CMD_WRDI     (unsigned)0x04
#define EEPROM_CMD_WREN     (unsigned)0x06
#define EEPROM_CMD_RDSR     (unsigned)0x05
#define EEPROM_CMD_WRSR     (unsigned)0x01

struct STATREG {
	BYTE WIP : 1;
	BYTE WEL : 1;
	BYTE BP0 : 1;
    BYTE    BP1 : 1;
    BYTE    RESERVED : 3;
    BYTE    WPEN : 1;
};


union _EEPROMStatus_
{
    struct STATREG  Bits;
    BYTE Char;
};

int EEPROMInit(void);
union _EEPROMStatus_ EEPROMReadStatus(void);
void EEPROMWriteByte(BYTE data, WORD address);
BYTE EEPROMReadByte(WORD address);
void EEPROMWriteWord(WORD data, WORD address);
WORD EEPROMReadWord(WORD address);
void EEPROMWriteEnable(void);
int EEPROMWriteArray(DWORD address, void *pData, WORD nCount);
int EEPROMReadArray(WORD address, void *pData, WORD nCount);
void SPI_TxRx(BYTE * TxData, BYTE * RxData, int Len);

#endif	/* EEPROM_H */

