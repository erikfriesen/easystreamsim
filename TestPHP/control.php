<?php
$method = $_SERVER['REQUEST_METHOD'];

function print_data($format) {

	global $data, $response_code;
	//set http response type
	http_response_code($response_code);

	switch($format) {

		case "json":
			print json_encode($data);
			break;
		case "jsonnumber":
			print json_encode($data, JSON_NUMERIC_CHECK);
			break;

		default:
			http_response_code(500);
			print("Failure to print data in data_h.php");
	}
}
$testresponse = false;//Test Flag indicating that we are sending new changed data since we aren't connect to a db

$testjson = '{"Config":{"Info":{"DeviceName":"Test device","DeviceType":"streamer","DeviceSerial":"000:AAA:BBB","AuthToken":"0e711b97-67b0-4592-91fa-437b95a64646","Streams":[{"Status":"Off","Connected":"0","CtrlStream":"0"},{"Status":"Off","Connected":"0","CtrlStream":"0"}],"PlayState":"Off","EncType":"0","CtrlStreamIn":"0","DspError":"0","AudioPeak":"-27.-1","AudioLevel":"-27.-1","PlayerBuffer":"0","Unixtime":"1508188412","JsonVersion":"1.00","ControlStatus":"Disconnected","Version":"2.000"},"Player":{"CtrlStreamIn":"0","CtrlRxSelection":"1","PlayState":"Off","RxStreams":[{"Name":"Aleph test","Username":"","Password":"","Url":"http://icecast.aleph-com.net:8000/9235230.mp3"},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""},{"Name":"","Username":"","Password":"","Url":""}]},"Stream":{"ControlType":"0","BitRate":"64","EncType":"0","StreamOnLevel":"2","StreamOnDelay":"300","Mounts":[{"StreamActive":"1","CtrlStream":"0","StreamConnected":"0","AuthError":"0","SourceName":"friesen.mp2","UserName":"source","Password":"hackme","Name":"8442740","URL":"192.168.0.100","Port":"8000"},{"StreamActive":"0","CtrlStream":"0","StreamConnected":"0","AuthError":"0","SourceName":"friesen.mp2","UserName":"source","Password":"hackme","Name":"8442741","URL":"192.168.0.100","Port":"8000"}]},"Services":{"UpdateUrl":"http://easylisten.net/firmware.bin","DoUpdate":"0","ControlUrl":"http://192.168.0.120/easystreamsim/control.php","RemoteControl":"1","PreAuthToken":"a7cd5406-fa8b-4e1c-99d5-742255c796e6","PollPeriod":"5","UpdateStatus":"done","ControlStatus":"Disconnected"},"Hardware":{"HardwareGain":"0","SampleRate":"1","InVolume":"0.000000","OutVolume":"0","AutoLimiter":"1"},"DSP":{"Type":"Symetrix","IP":"192.168.0.210","Port":"48630","TCP":"0","Stream1":"5000","Stream2":"5001","StreamIn":"5002","AudioLevel":"5005","IsStreaming1":"5006","IsStreaming2":"5007","AudioPeak":"5004","RxSelection":"5003","Error":"0"}}}';
try {
	switch ($method) {
		default:
			throw new Exception("Non allowed access");
		case "POST":
			$jsoninfo = json_decode(file_get_contents('php://input'), false);
			if (isset($jsoninfo->Provision)) {
				//Provisioning code here
				$data->ProvisionResponse["DeviceSerial"] = $jsoninfo->Provision->DeviceSerial;
				$data->ProvisionResponse["AuthToken"] = "0e711b97-67b0-4592-91fa-437b95a64646";
			} else {
				//Ack here
				$ack = false;
				if (isset($jsoninfo->Info)) {
					//Standard information
					if ($jsoninfo->Info->AuthToken == "0e711b97-67b0-4592-91fa-437b95a64646") {
						if ($testresponse == true) {
							//This scenario says we have newer data that needs to sync to easystream
							//Copied data back to itself, we don't have buffered data, sorry this isn't right
							$newjson = json_decode($testjson);
							$data = $newjson;
							$data->Config->Info->DeviceSerial = $jsoninfo->Info->DeviceSerial;
							$data->Config->Info->Unixtime = time();
							if ($data->Config->Player->CtrlStreamIn == '1') {
								$data->Config->Player->CtrlStreamIn = '0';
							} else {
								$data->Config->Player->CtrlStreamIn = '1';
							}
						} else {
							//Everything ok in this scenario, all in sync
							//Copied data back to itself
							$data->Ack["Auth"] = "true";
							$data->Ack["DeviceSerial"] = $jsoninfo->Info->DeviceSerial;
							$data->Ack["Unixtime"] = $jsoninfo->Info->Unixtime;
						}
					} else {
						//Non authorized, so we don't send them any information
						$data->Ack["Auth"] = "false";
					}
				} else if (isset($jsoninfo->Config)) {
					if($testresponse == true){
						$LastDbUnixTime = time();
					} else {
						$LastDbUnixTime = 0;
					}
					//New incoming config					
					//otherwise send newer records back if unixtime < ours, else ack = true
					if ($jsoninfo->Config->Info->Unixtime > $LastDbUnixTime) {
						//Update our records here if unixtime > ours
						//Copied data back to itself
						$data->Ack["Auth"] = "true";
						$data->Ack["DeviceSerial"] = $jsoninfo->Config->Info->DeviceSerial;
						$data->Ack["Unixtime"] = $jsoninfo->Config->Info->Unixtime;
					} else if ($jsoninfo->Config->Info->Unixtime < $LastDbUnixTime) {
						//send newer records back if unixtime < ours
						$newjson = json_decode($testjson);
						//print_r($newjson);
						$data = $newjson;
						$data->Config->Info->DeviceSerial = $jsoninfo->Config->Info->DeviceSerial;
						$data->Config->Info->Unixtime = $LastDbUnixTime;
						$data->Config->Player->CtrlStreamIn = time() % 2;
					} else {
						//Standard ack
						//Copied data back to itself
						$data->Ack["Auth"] = "true";
						$data->Ack["DeviceSerial"] = $jsoninfo->Config->Info->DeviceSerial;
						$data->Ack["Unixtime"] = $jsoninfo->Config->Info->Unixtime;
					}
				} else {
					throw new Exception("Bad request");
				}
			}

			break;
	}
} catch (Exception $e) {
	//$dbh->rollBack();
	$data = $e->getMessage();
	$response_code = 500;
}

print_data("json");