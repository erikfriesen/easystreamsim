#include <errno.h>
#include <pthread.h>
#include <syslog.h>
#include <arpa/inet.h>
#include <netdb.h> 
#include <math.h>
#include "include/GenericTypeDefs.h"
#include "global.h"
#include "iceconfig.h"
#include <string.h>
#include <stdio.h>


typedef struct {
	unsigned int HeadPtr : PcmBufferBits;
	unsigned int TailPtr : PcmBufferBits;
} _PTR;

volatile _PTR RxPtr;
volatile short * PcmBuffer;
volatile int DecodeError = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void * PlaybackAudioThread(void * ptr) {
	int a, b, err, c, d;
	//Audio variables
	int PlayState;
	//time_t timer, comparetimer;
	//TCP variables
	unsigned int rrate = StdSampleRate;
	struct {
		unsigned int Ready : PcmBufferBits;
	} VAR;

	while (1) {
		usleep(10000); //Give threads a breather
		if (PlayerSetup.Ctrl_StreamIn) {
			int index = PlayerSetup.Ctrl_StreamIn - 1;
			sprintf((char*) PlayerSetup.PlayState, "Connected to %s", PlayerSetup.RxStreams[index].Name);
		} else {
			sprintf((char*) PlayerSetup.PlayState, "Off");
		}
	}
}